$(document).ready(function(){
	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy'
	});

	// Tabel Rayon
	var table_setor_bank = $('#table_setor_bank').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'setor_bank/get_setor_bank',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 2 ],
	      "orderable": false,
	  }
	  ],
	});

	// Tabel Detail Rayon
	var table_detail_setor_bank = $('#table_detail_setor_bank').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'setor_bank/get_detail_setor_bank',
	      "type": "POST",
	      "data": function(data){
	      	data.tgl_bayar = $('#tgl_bayar').val()
	      }
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  }
	  ],
	});

	// Button Detail Setor Bank
	$('body').on('click', '.btn_detail_setor_bank', function(e){
		e.preventDefault();

		let id_bayar = $(this).attr('id').split('_')[1];

		let tgl_bayar = $('#tgl_bayar_'+id_bayar).val();

		$('#tgl_bayar').val(tgl_bayar);
		table_detail_setor_bank.ajax.reload();
		$('#detail_setor_bank_modal').modal('show');
	})

	// Before Setor Bank
	$('body').on('click','.before_setor_bank', function(e){
		e.preventDefault();

		let tgl_bayar = $(this).attr('id').split('_')[1];

		$('#tgl_bayar_setor').val(tgl_bayar);

		$('#setor_bank_modal').modal('show');
	})

	// Form Setor Bank
	$('#form_setor_bank').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#_error_'+i).html(val);
						});
					}else{
						$('#setor_bank_modal').modal('hide');
						$('#form_setor_bank')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_setor_bank.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	})

	// Form Setor Bank
	$('body').on('click', '.btn_batalkan_setor_bank', function(e){
		e.preventDefault();

		let tgl_bayar = $(this).attr('id').split('_')[1];

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: base_url+'setor_bank/batalkan',
			type: 'POST',
			data: {
				tgl_bayar: tgl_bayar
			},
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					if (res.status == 'success') {
						swal('Success', res.message, 'success');
						table_setor_bank.ajax.reload();
					}else{
						swal('Error', res.message, 'error');
					}
				},1000);
			}
		});
	})
});