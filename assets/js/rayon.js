$(document).ready(function(){
	// Tabel Rayon
	var table_rayon = $('#table_rayon').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rayon/get_rayon',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0,2,3,4,5,6 ],
	      "orderable": false,
	  }
	  ],
	});

	// Tabel Detail Rayon
	var table_detail_rayon = $('#table_detail_rayon').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rayon/get_detail_rayon',
	      "type": "POST",
	      "data": function(data){
	      	data.id_rayon_ref = $('#id_rayon_ref').val()
	      }
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  }
	  ],
	});

	// Button Detail Rayon
	$('body').on('click', '.btn_detail_rayon', function(e){
		e.preventDefault();

		let id_rayon_ref = $(this).attr('id').split('_')[1];

		$('#id_rayon_ref').val(id_rayon_ref);

		$('#loading').modal('show');
		$.get('rayon/get_timeline_rekap/'+id_rayon_ref, function(data){
			setTimeout(function(){
				$('#timelinerekap').html(data);
				table_detail_rayon.ajax.reload();
				$('#loading').modal('hide');
				$('#detail_rayon_modal').modal('show');
			},500);
		})

	})

	// Form Menambah Rayon
	$('#form_add_rayon').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#add_rayon_modal').modal('hide');
						$('#form_add_rayon')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_rayon.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	});

	// Before Edit Rayon
	$('body').on('click', '.before_edit_rayon', function(e){
		e.preventDefault();

		let id_rayon = $(this).attr('id').split('_')[1];

		$.ajax({
			url: base_url+'rayon/get_detail_edit',
			type: 'POST',
			data: {
				id_rayon: id_rayon
			},
			dataType: 'json',
			success: function(res){
				if (res.status == 'success') {
					$.each(res.data, function(i,val){
						$('#'+i).val(val);
					});
					$('#edit_rayon_modal').modal('show');
				}else{
					swal('Error', res.message, 'error');
				}
			}
		});
	});

	// Form Update Rayon
	$('#form_update_rayon').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#_error_'+i).html(val);
						});
					}else{
						$('#edit_rayon_modal').modal('hide');
						$('#form_update_rayon')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_rayon.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	})

	$('#add_rayon_modal').on('hidden.bs.modal', function(){
		$('form')[0].reset();
	})

	// Before delete rayon
	$('body').on('click', '.before_delete_rayon', function(e){
		e.preventDefault();

		let id_rayon = $(this).attr('id').split('_')[1];

		$('#delete_id_rayon').val(id_rayon);
		$('#delete_rayon_modal').modal('show');
	});

	// Delete Rayon
	$('#form_delete_rayon').on('submit', function(e){
		e.preventDefault();

		const data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('#delete_rayon_modal').modal('hide');

					if (res.status == 'success') {
						swal('Success', res.message, 'success');
						table_rayon.ajax.reload();
					}else{
						swal('Error', res.message, 'error');
					}
				},500);
			}
		});
	})
});