$(document).ready(function(){
	// Tabel Wp
	var table_rekap_belum_bayar = $('#table_rekap_belum_bayar').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rekap_belum_bayar/get_rekap',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  }
	  ],
	});
})