$(document).ready(function(){
	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy'
	});

	// Tabel Wp
	var table_wp = $('#table_wp').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'wp/get_wp',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 2 ],
	      "orderable": false,
	  }
	  ],
	});

	// Form Menambah Wp
	$('#form_add_wp').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#add_wp_modal').modal('hide');
						$('#form_add_wp')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_wp.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	});

	// Form Import Wp
	$('#form_import_wp').on('submit', function(e){
		e.preventDefault();

		let data = new FormData(this);

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			cache: false,
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#import_wp_modal').modal('hide');
						$('#form_import_wp')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_wp.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	});

	// Before Edit Wp
	$('body').on('click', '.before_edit_wp', function(e){
		e.preventDefault();

		let id_wp = $(this).attr('id').split('_')[1];
		$('.before_delete_wp').attr('id', 'deletewp_'+id_wp);

		$.ajax({
			url: base_url+'wp/get_detail_edit',
			type: 'POST',
			data: {
				id_wp: id_wp
			},
			dataType: 'json',
			success: function(res){
				if (res.status == 'success') {
					$('#id_rt_ref').html(res.rt);
					$.each(res.data, function(i,val){
						$('#'+i).val(val);
					});
					$('#edit_wp_modal').modal('show');
				}else{
					swal('Error', res.message, 'error');
				}
			}
		});
	});

	// Form Update Wp
	$('#form_update_wp').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#_error_'+i).html(val);
						});
					}else{
						$('#edit_wp_modal').modal('hide');
						$('#form_update_wp')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_wp.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	})

	// Before Bayar WP
	$('body').on('click','.before_bayar_wp', function(e){
		e.preventDefault();

		let id_wp = $(this).attr('id').split('_')[1];

		$('#_id_wp').val(id_wp);

		$('#bayar_wp_modal').modal('show');
	})

	// Form Bayar
	$('#form_bayar_wp').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#_error_'+i).html(val);
						});
					}else{
						$('#bayar_wp_modal').modal('hide');
						$('#form_bayar_wp')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_wp.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	})

	// Before Batal Bayar WP
	$('body').on('click','.before_batal_bayar_wp', function(e){
		e.preventDefault();

		let id_wp = $(this).attr('id').split('_')[1];

		$('#batal_id_wp').val(id_wp);

		$('#batal_bayar_wp_modal').modal('show');
	})

	// Form Batal Bayar
	$('#form_batal_bayar_wp').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					$('#batal_bayar_wp_modal').modal('hide');
					$('#form_batal_bayar_wp')[0].reset();
					if (res.status == 'success') {
						swal('Success', res.message, 'success');
						table_wp.ajax.reload();
					}else{
						swal('Error', res.message, 'error');
					}
				},1000);
			}
		});
	})

	// Before Delete WP
	$('body').on('click','.before_delete_wp', function(e){
		e.preventDefault();

		let id_wp = $(this).attr('id').split('_')[1];

		$('.btn_delete_wp').attr('id','deletewp_'+id_wp);

		$('#delete_wp_modal').modal('show');
	})

	// Delete WP
	$('body').on('click', '.btn_delete_wp', function(e){
		e.preventDefault();

		let id_wp = $(this).attr('id').split('_')[1];

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: base_url+'wp/delete',
			type: 'POST',
			data: {
				id_wp: id_wp
			},
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');

					$('#delete_wp_modal').modal('hide');
					$('#edit_wp_modal').modal('hide');
					if (res.status == 'success') {
						swal('Success', res.message, 'success');
						table_wp.ajax.reload();
					}else{
						swal('Error', res.message, 'error');
					}
				},1000);
			}
		})
	})

	$('#add_wp_modal').on('hidden.bs.modal', function(){
		$('form')[0].reset();
		$('#_id_rt_ref').attr('disabled', true);
	})

	$('body').on('change', '#id_rayon', function(e){
		e.preventDefault();
		let id_rayon = $(this).val();

		if (id_rayon != '') {
			$.get(base_url+'wp/get_rt_option/'+id_rayon, function(data){
				$('#id_rt_ref').html(data);
				$('#id_rt_ref').attr('disabled', false);
			})
		}else{
			$('#id_rt_ref').html('<option value="">Pilih RT</option>');
			$('#id_rt_ref').attr('disabled', true);
		}

	})

	$('body').on('change', '#_id_rayon', function(e){
		e.preventDefault();
		let id_rayon = $(this).val();

		if (id_rayon != '') {
			$.get(base_url+'wp/get_rt_option/'+id_rayon, function(data){
				$('#_id_rt_ref').html(data);
				$('#_id_rt_ref').attr('disabled', false);
			})
		}else{
			$('#_id_rt_ref').html('<option value="">Pilih RT</option>');
			$('#_id_rt_ref').attr('disabled', true);
		}

	})
});