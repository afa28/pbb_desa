$(document).ready(function(){
	// Tabel Wp
	var table_rekap_lunas = $('#table_rekap_lunas').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rekap_lunas/get_rekap',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  }
	  ],
	});
})