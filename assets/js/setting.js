$(document).ready(function(){
	$('#form_update_setting').on('submit', function(e){
		e.preventDefault();

		const data = new FormData(this);

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			cache: false,
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						if (res.status == 'success') {
							swal('Success', res.message, 'success');

							if (res.logo_surat != '') {
								$('#logo_surat').html('<img src="'+base_url+'assets/logo/'+res.logo_surat+'">');
							}
						}else{
							swal('Error', res.message, 'error');
						}
					}
				}, 500);
			}
		})
	})
})