$(document).ready(function(){
	$('.datepicker').datepicker({
		autoclose: true,
		format: 'dd/mm/yyyy'
	});

	// rayon on change
	$('body').on('change', '#id_rayon_ref', function(e){
		e.preventDefault();

		let id_rayon_ref = $(this).val();

		if (id_rayon_ref == '') {
			$('#id_wp').html('<option value="">Pilih Nama Wajib Pajak</option>');
			return false;
		}

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: base_url+'input_pembayaran/get_wp_option',
			type: 'POST',
			data: {
				id_rayon_ref: id_rayon_ref
			},
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					if (res.status == 'success') {
						$('#id_wp').html(res.option);
						$('#id_wp').select2();
					}else{
						swal('Error', res.message, 'error');
					}
				})
			}
		})
	})

	// id wp on change
	$('body').on('change', '#id_wp', function(e){
		e.preventDefault();

		let id_wp = $(this).val();

		if (id_wp == '') {
			$('#nomor_wp').html('');
			$('#pagu_wp').html('');
			return false;
		}

		$.ajax({
			url: base_url+'input_pembayaran/get_wp_detail',
			type: 'POST',
			data: {
				id_wp: id_wp
			},
			dataType: 'json',
			success: function(res){
				if (res.status == 'success') {
					$('#nomor_wp').val(res.data.nomor_wp);
					$('#pagu_wp').val(res.data.pagu_wp);

					if (res.data.status == 2) {
						$('#alert_lunas').show();
						$('#btn_bayar').attr('disabled', true);
					}else{
						$('#alert_lunas').hide();
						$('#btn_bayar').attr('disabled', false);
					}
				}else{
					swal('Error', res.message, 'error');
				}
			}
		})
	})

	// Form Bayar
	$('#form_bayar_wp').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#form_bayar_wp')[0].reset();
						$('#id_wp').html('<option value="">Pilih Nama Wajib Pajak</option>');
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	})
})