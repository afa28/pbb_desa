$(document).ready(function(){
	// Form Change Password
	$('#form_change_password').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#form_change_password')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							setTimeout(function(){
								window.location.href = base_url+'login/logout';
							},1000);
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	});
})