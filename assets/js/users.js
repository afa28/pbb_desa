$(document).ready(function(){
	// Tabel User
	var table_users = $('#table_users').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'users/get_users',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 3 ],
	      "orderable": false,
	  }
	  ],
	});

	// Form Menambah User
	$('#form_add_users').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#add_users_modal').modal('hide');
						$('#form_add_users')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_users.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	});

	// Button Reset Password
	$('body').on('click', '.btn_reset_password', function(e){
		e.preventDefault();

		let id_login = $(this).attr('id').split('_')[1];

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: base_url+'users/reset_password',
			type: 'POST',
			data: {
				id_login: id_login
			},
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					if (res.status == 'success') {
						swal('Success', res.message, 'success');
						table_users.ajax.reload();
					}else{
						swal('Error', res.message, 'error');
					}
				},1000);
			}
		});
	})
});