$(document).ready(function(){
	// Form Login
	$('#form_login').on('submit', function(e){
		e.preventDefault();

		const data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					console.log(res);
					$('#loading').modal('hide');
					$('.text-danger').html('');
					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						if (res.status == 'success') {
							$('#notif').addClass('alert-success');
							$('#notif').removeClass('alert-danger');
							$('#notif').html(res.message);
							$('#notif').show('fade');
							window.location.href = base_url;
						}else{
							$('#notif').addClass('alert-danger');
							$('#notif').removeClass('alert-success');
							$('#notif').html(res.message);
							$('#notif').show('fade');
							setTimeout(function(){
								$('#notif').hide('fade');
							},3000);
						}
					}
				},1000);
			}
		})
	})
})