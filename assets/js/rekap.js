$(document).ready(function(){
	$('#tgl_bayar').daterangepicker({
		autoclose: true
	});

	$('#tgl_setor').daterangepicker({
		autoclose: true
	});

	// Tabel Wp
	var table_rekap = $('#table_rekap').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rekap/get_rekap',
	      "type": "POST",
	      "data": function(data)
	      {
	      	data.tipe_rekap = $('#tipe_rekap').val(),
	      	data.id_rayon_ref = $('#id_rayon_ref').val(),
	      	data.id_rt_ref = $('#id_rt_ref').val(),
	      	data.bulan_bayar = $('#bulan_bayar').val()
	      	data.tgl_bayar = $('#tgl_bayar').val(),
	      	data.tgl_setor = $('#tgl_setor').val()
	      }
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  }
	  ],
	});

	$('body').on('change', '#id_rayon_ref', function(e){
		e.preventDefault();

		let id_rayon_ref = $(this).val();
		if (id_rayon_ref == '') {
			$('#id_rt_ref').html('<option value="">Pilih Wilayah</option>');
			return false;
		}

		$.get('rekap/get_option_wilayah/'+id_rayon_ref, function(data){
			$('#id_rt_ref').html(data);
		});
	})

	$('body').on('click', '.btn_filter', function(e){
		e.preventDefault();

		table_rekap.ajax.reload();
	});

	$('body').on('click', '.btn_change_filter', function(e){
		e.preventDefault();

		let tipe = $(this).attr('id');
		$('#form_download_rekap')[0].reset();
		$('.btn_change_filter').removeClass('btn-success');
		$(this).addClass('btn-success');
		$('#tipe_rekap').val(tipe);
		$('.area').hide();	

		if (tipe == 'filter_penerimaan_rayon') {
			$('#area_id_rayon_ref').show();
			$('#area_id_rt_ref').show();
		}else if (tipe == 'filter_bulan_rayon') {
			$('#area_id_rayon_ref').show();
			$('#area_bulan').show();
		}else if (tipe == 'filter_tgl_rayon') {
			$('#area_id_rayon_ref').show();
			$('#area_tgl_bayar').show();
		}else if(tipe == 'filter_tgl_bayar'){
			$('#area_tgl_bayar').show();
		}else{
			$('#area_tgl_setor').show();
		}
	});

	$('body').on('click', '.btn_rekap', function(e){
		e.preventDefault();

		let tipe_rekap = $(this).attr('id').split('-')[1];
		$('#tipe_rekap').val(tipe_rekap);
		$('.divisi_rekap').hide();
		$('.btn_rekap').removeClass('btn-primary');
		$(this).addClass('btn-primary');

		if (tipe_rekap == 'rayon') {
			$('#rekap_rayon').show();
		}else if(tipe_rekap == 'tgl_bayar'){
			$('#rekap_tgl_bayar').show();
		}else{
			$('#rekap_tgl_setor').show();
		}

		table_rekap.ajax.reload();
	});
});