$(document).ready(function(){
	// Tabel RT
	var table_rt = $('#table_rt').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rt/get_rt',
	      "type": "POST"
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 2 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 3 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 4 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 5 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 6 ],
	      "orderable": false,
	  },
	  {
	      "targets": [ 7 ],
	      "orderable": false,
	  }
	  ],
	});

	// Tabel Detail RT
	var table_detail_rt = $('#table_detail_rt').DataTable({
	  responsive: true,
	  "processing": true,
	  "serverSide": true,
	  "order": [],
	  "ajax": {
	      "url": base_url+'rt/get_detail_rt',
	      "type": "POST",
	      "data": function(data){
	      	data.id_rt = $('#id_rt_').val()
	      }
	  },
	  "columnDefs": [
	  {
	      "targets": [ 0 ],
	      "orderable": false,
	  }
	  ],
	});

	// Button Detail RT
	$('body').on('click', '.btn_detail_rt', function(e){
		e.preventDefault();

		let id_rt_ref = $(this).attr('id').split('_')[1];

		$('#id_rt_').val(id_rt_ref);
		table_detail_rt.ajax.reload();
		$('#detail_rt_modal').modal('show');
	})

	// Form Menambah RT
	$('#form_add_rt').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#error_'+i).html(val);
						});
					}else{
						$('#add_rt_modal').modal('hide');
						$('#form_add_rt')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_rt.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	});

	// Before Edit RT
	$('body').on('click', '.before_edit_rt', function(e){
		e.preventDefault();

		let id_rt = $(this).attr('id').split('_')[1];

		$.ajax({
			url: base_url+'rt/get_detail_edit',
			type: 'POST',
			data: {
				id_rt: id_rt
			},
			dataType: 'json',
			success: function(res){
				if (res.status == 'success') {
					$.each(res.data, function(i,val){
						$('#'+i).val(val);
					});
					$('#edit_rt_modal').modal('show');
				}else{
					swal('Error', res.message, 'error');
				}
			}
		});
	});

	// Form Update RT
	$('#form_update_rt').on('submit', function(e){
		e.preventDefault();

		let data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('.text-danger').html('');

					if (res.status == 'valid_error') {
						$.each(res.error, function(i,val){
							$('#_error_'+i).html(val);
						});
					}else{
						$('#edit_rt_modal').modal('hide');
						$('#form_update_rt')[0].reset();
						if (res.status == 'success') {
							swal('Success', res.message, 'success');
							table_rt.ajax.reload();
						}else{
							swal('Error', res.message, 'error');
						}
					}
				},1000);
			}
		});
	})

	$('#add_rt_modal').on('hidden.bs.modal', function(){
		$('form')[0].reset();
	})

	// Before delete RT
	$('body').on('click', '.before_delete_rt', function(e){
		e.preventDefault();

		let id_rt = $(this).attr('id').split('_')[1];

		$('#delete_id_rt').val(id_rt);
		$('#delete_rt_modal').modal('show');
	});

	// Delete RT
	$('#form_delete_rt').on('submit', function(e){
		e.preventDefault();

		const data = $(this).serialize();

		$.ajax({
			beforeSend: function(){
				$('#loading').modal('show');
			},
			url: $(this).attr('action'),
			type: 'POST',
			data: data,
			dataType: 'json',
			success: function(res){
				setTimeout(function(){
					$('#loading').modal('hide');
					$('#delete_rt_modal').modal('hide');

					if (res.status == 'success') {
						swal('Success', res.message, 'success');
						table_rt.ajax.reload();
					}else{
						swal('Error', res.message, 'error');
					}
				},500);
			}
		});
	})
});