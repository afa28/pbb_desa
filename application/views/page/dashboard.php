
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-layers"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count"><?=$total_sppt ? $total_sppt : 0?></span> lembar</div>
                                    <div class="stat-heading">SPPT</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-4">
                                <i class="ti-alert"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count"><?=$total_sppt_terhutang ? $total_sppt_terhutang : 0?></span> lembar</div>
                                    <div class="stat-heading">SPPT Terhutang</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-check-box"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count"><?=$total_sppt_terbayar ? $total_sppt_terbayar : 0?></span> lembar</div>
                                    <div class="stat-heading">SPPT Terbayar</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Widgets -->
        <!-- Widgets  -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-3">
                                <i class="ti-money"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">Rp<span class="count"><?=$total_pagu ? $total_pagu : 0?></span></div>
                                    <div class="stat-heading">Total Pagu</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-4">
                                <i class="ti-alert"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">Rp<span class="count"><?=$pagu_terhutang ? $pagu_terhutang : 0?></span></div>
                                    <div class="stat-heading">Pagu Terhutang</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="ti-check-box"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text">Rp<span class="count"><?=$pagu_terbayar ? $pagu_terbayar : 0?></span></div>
                                    <div class="stat-heading">Pagu Terbayar</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Widgets -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-3">
                                <i class="ti-user"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count"><?=$total_rayon ? $total_rayon : 0?></span></div>
                                    <div class="stat-heading">Rayon</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="ti-home"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count"><?=$total_rt ? $total_rt : 0?></span></div>
                                    <div class="stat-heading">RT</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="title-header">
                                    Riwayat Pembayaran
                                </div>
                            </div>
                            <div class="card-body">
                                <?php if (isset($riwayat_pembayaran) && is_array($riwayat_pembayaran) && count($riwayat_pembayaran) > 0): ?>
                                    <?php foreach ($riwayat_pembayaran as $key => $value): ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="stat-widget-five">
                                                    <div class="stat-icon dib flat-color-3">
                                                        <i class="ti-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8" style="font-size: 80%">
                                                <?=$value['nama_wp']?>  <span class="badge badge-success float-right">Rp. <?=rupiah($value['pagu_wp'])?></span><br>
                                                Telah bayar pada <?=tgl_indo($value['tgl_bayar'])?>
                                            </div>
                                        </div>
                                        <hr>
                                    <?php endforeach ?>
                                    <a href="<?=base_url()?>rekap_lunas" class="btn btn-info btn-block">Lihat Selengkapnya</a>
                                <?php else: ?>
                                    Tidak ada data
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="title-header">
                                    Nama Rayon
                                </div>
                            </div>
                            <div class="card-body">
                                <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                                    <div class="row">
                                    <?php foreach ($rayon as $key => $value): ?>
                                            <div class="col-md-3">
                                                <img class="user-avatar rounded-circle" src="<?=base_url()?>assets/images/admin.jpg" alt="User Avatar"><br>
                                                <span><?=$value['nama_rayon']?></span>
                                            </div>
                                    <?php endforeach ?>
                                    </div>
                                    <a href="<?=base_url()?>rayon" class="btn btn-info btn-block">Lihat Selengkapnya</a>
                                <?php else: ?>
                                    Tidak ada data
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="title-header">
                                    Pencapaian Rayon
                                </div>
                            </div>
                            <div class="card-body">
                                <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                                    <?php foreach ($rayon as $key => $value): ?>
                                        <?php 
                                            // Total Bayar
                                            $query_bayar = "SELECT sum(w.pagu_wp) as total_bayar, COUNT(w.id_wp) as jumlah_wp_terbayar FROM tb_wp w
                                                            JOIN tb_rt rt ON rt.id_rt=w.id_rt_ref
                                                            WHERE rt.id_rayon_ref = ? AND w.status = ?";
                                            $result = $this->db->query($query_bayar, array($value['id_rayon'], 2))->row_array();

                                            $total_bayar = $result['total_bayar'];
                                            $jumlah_wp_terbayar = $result['jumlah_wp_terbayar'];
                                            $total_pagu = $value['total_pagu'];

                                            // Total Kekurangan
                                            $total_kekurangan = $total_pagu-$total_bayar;

                                            if ($total_bayar > 0 && $total_pagu > 0) {
                                                $persen_pagu_terpenuhi = round($total_bayar/$total_pagu*100,2);
                                            }else{
                                                $persen_pagu_terpenuhi = 0;
                                            }
                                         ?>
                                        <?=$value['nama_rayon']?> <span class="badge badge-default float-right"><?=$jumlah_wp_terbayar?>/<?=$value['jumlah_wp']?></span><br>
                                        <div class="progress">
                                          <div class="progress-bar" role="progressbar" style="width: <?=$persen_pagu_terpenuhi?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?=$persen_pagu_terpenuhi?>%</div>
                                        </div>
                                        <hr>
                                    <?php endforeach ?>
                                    <a href="<?=base_url()?>rayon" class="btn btn-info btn-block">Lihat Selengkapnya</a>
                                <?php else: ?>
                                    Tidak ada data
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <!-- /#add-category -->
    </div>
    <!-- .animated -->
</div>
<!-- /.content