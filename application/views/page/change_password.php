<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Change Password</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">Change Password</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-6 offset-3">
                <div class="card">
                    <div class="card-header">
                        <strong>Ubah Password</strong>
                    </div>
                    <div class="card-body card-block">
                        <?=form_open('change_password/update','id="form_change_password"')?>
                            <div class="form-group">
                                <label class=" form-control-label">Password Lama</label>
                                <input type="password" name="old_login_password" placeholder="Isikan Password Lama" class="form-control">
                                <div class="text-danger" id="error_old_login_password"></div>
                            </div>
                            <div class="form-group">
                                <label class=" form-control-label">Password Baru</label>
                                <input type="password" name="new_login_password" placeholder="Isikan Password Baru" class="form-control">
                                <div class="text-danger" id="error_new_login_password"></div>
                            </div>
                            <div class="form-group">
                                <label class=" form-control-label">Password Baru Ulang</label>
                                <input type="password" name="renew_login_password" placeholder="Ulangi Password Baru" class="form-control">
                                <div class="text-danger" id="error_renew_login_password"></div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">
                                Ubah Password
                            </button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->