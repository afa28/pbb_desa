<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Cetak Rekap</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">Cetak Rekap</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Cetak Rekap</strong>
                    </div>
                    <div class="card-body">
                        <?=form_open('rekap/download', 'id="form_download_rekap"')?>
                            <?php if ($this->session->flashdata('input_error')): ?>
                                <div class="alert alert-warning"><?=$this->session->flashdata('input_error')?></div>
                            <?php endif ?>
                            <button class="btn btn-success btn_change_filter" id="filter_penerimaan_rayon">Penerimaan Rayon</button>
                            <button class="btn btn-default btn_change_filter" id="filter_bulan_rayon">Bulan Rayon</button>
                            <button class="btn btn-default btn_change_filter" id="filter_tgl_rayon">Tanggal Rayon</button>
                            <button class="btn btn-default btn_change_filter" id="filter_tgl_bayar">Tanggal Bayar</button>
                            <button class="btn btn-default btn_change_filter" id="filter_tgl_setor">Penyetoran Bank</button>
                                
                            <hr>
                            <input type="hidden" id="tipe_rekap" name="tipe_rekap" value="filter_penerimaan_rayon">
                            <div class="form-group area" id="area_id_rayon_ref">
                                <label>Rayon</label>
                                <select class="form-control select2" id="id_rayon_ref" name="id_rayon_ref">
                                    <option value="">Pilih Rayon</option>
                                    <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                                        <?php foreach ($rayon as $key => $value): ?>
                                            <option value="<?=$value['id_rayon']?>"><?=$value['nama_rayon']?></option>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>
                            <div class="form-group area" id="area_id_rt_ref">
                                <label>Wilayah</label>
                                <select class="form-control" id="id_rt_ref" name="id_rt_ref">
                                    <option value="">Pilih Wilayah</option>
                                </select>
                            </div>

                            <div class="form-group area" id="area_bulan" style="display: none;">
                                <label>Bulan</label>
                                <select class="form-control" name="bulan_bayar" id="bulan_bayar">
                                    <option value="">Pilih Bulan</option>
                                    <?php if (isset($tahun_bayar) && is_array($tahun_bayar) && count($tahun_bayar) > 0): ?>
                                        <?php foreach ($tahun_bayar as $key => $value): ?>
                                            <?php for($bulan = 1; $bulan <= 12; $bulan++): ?>
                                                <option value="<?=$bulan?>"><?=date('F Y', strtotime($value['tahun'].'-'.$bulan.'-1'))?></option>
                                            <?php endfor; ?>
                                        <?php endforeach ?>
                                    <?php else: ?>
                                        <?php for($bulan = 1; $bulan <= 12; $bulan++): ?>
                                            <option value="<?=$bulan?>"><?=date('F Y', strtotime(date('Y').'-'.$bulan.'-1'))?></option>
                                        <?php endfor; ?>
                                    <?php endif ?>
                                </select>
                            </div>

                            <div class="form-group area" style="display: none" id="area_tgl_bayar">
                                <label>Tanggal Bayar</label>
                                <input type="text" id="tgl_bayar" name="tgl_bayar" placeholder="Tanggal Bayar" class="form-control">
                            </div>

                            <div class="form-group area" style="display: none" id="area_tgl_setor">
                                <label>Tanggal Setor</label>
                                <input type="text" id="tgl_setor" name="tgl_setor" placeholder="Tanggal Setor" class="form-control">
                            </div>

                            <button type="button" class="btn btn-info btn_filter"><i class="fa fa-filter"></i> Filter</button>
                            <button type="submit" title="Download Rekap" class="btn btn-success"><i class="fa fa-download"></i> Download</button>
                        </form>
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="table_rekap" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor SPPT</th>
                                        <th>Nama Wajib Pajak</th>
                                        <th>Nama Rayon</th>
                                        <th>RT</th>
                                        <th>Total Pajak</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->