<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>SPPT</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">SPPT</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data SPPT</strong>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#add_wp_modal"><i class="fa fa-plus-circle"></i> Tambah</button>
                            </div>
                            <div class="col-md-5">
                                <a class="btn btn-success" href="<?=base_url()?>wp/export" target="_blank"><i class="fa fa-download"></i> Download Template</a>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#import_wp_modal"><i class="fa fa-upload"></i> Import SPPT</button>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="table_wp" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor SPPT</th>
                                        <th>Nama Wajib Pajak</th>
                                        <th>Nama Rayon</th>
                                        <th>RT</th>
                                        <th>Total Pajak</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="add_wp_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Tambah SPPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('wp/add', 'id="form_add_wp"')?>
            <div class="modal-body">
                <!-- row -->
                <div class="row">
                    <!-- col-6 -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor SPPT</label>
                            <input class="form-control" type="text" name="nomor_wp" placeholder="Nomor SPPT">
                            <div class="text-danger" id="error_nomor_wp"></div>
                        </div>
                        <div class="form-group">
                            <label>Nama Wajib Pajak</label>
                            <input class="form-control" type="text" name="nama_wp" placeholder="Nama Wajib Pajak">
                            <div class="text-danger" id="error_nama_wp"></div>
                        </div>
                        <div class="form-group">
                            <label>Total Pajak</label>
                            <input class="form-control" type="number" name="pagu_wp" placeholder="Total Pajak">
                            <div class="text-danger" id="error_pagu_wp"></div>
                        </div>
                    </div>
                    <!-- /col-6 -->
                    <!-- col-6 -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Rayon</label>
                            <select class="form-control" id="_id_rayon" name="id_rayon">
                                <option value="">Pilih Rayon</option>
                                <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                                    <?php foreach ($rayon as $key => $value): ?>
                                        <option value="<?=$value['id_rayon']?>"><?=$value['nama_rayon']?></option>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                            <div class="text-danger" id="error_id_rayon"></div>
                        </div>
                        <div class="form-group">
                            <label>RT</label>
                            <select class="form-control" id="_id_rt_ref" name="id_rt_ref" disabled="">
                                <option value="">Pilih RT</option>
                            </select>
                            <div class="text-danger" id="error_id_rt_ref"></div>
                        </div>
                    </div>
                    <!-- /col-6 -->
                </div>
                <!-- /row -->
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_wp_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Edit SPPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('wp/update', 'id="form_update_wp"')?>
            <div class="modal-body">
                <input type="hidden" name="id_wp" id="id_wp">
                <!-- row -->
                <div class="row">
                    <!-- col-6 -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor SPPT</label>
                            <input class="form-control" type="text" id="nomor_wp" name="nomor_wp" placeholder="Nomor SPPT">
                            <div class="text-danger" id="_error_nomor_wp"></div>
                        </div>
                        <div class="form-group">
                            <label>Nama Wajib Pajak</label>
                            <input class="form-control" type="text" id="nama_wp" name="nama_wp" placeholder="Nama Wajib Pajak">
                            <div class="text-danger" id="_error_nama_wp"></div>
                        </div>
                        <div class="form-group">
                            <label>Total Pajak</label>
                            <input class="form-control" type="number" id="pagu_wp" name="pagu_wp" placeholder="Total Pajak">
                            <div class="text-danger" id="_error_pagu_wp"></div>
                        </div>
                    </div>
                    <!-- /col-6 -->
                    <!-- col-6 -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Rayon</label>
                            <select class="form-control" id="id_rayon" name="id_rayon">
                                <option value="">Pilih Rayon</option>
                                <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                                    <?php foreach ($rayon as $key => $value): ?>
                                        <option value="<?=$value['id_rayon']?>"><?=$value['nama_rayon']?></option>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                            <div class="text-danger" id="_error_id_rayon"></div>
                        </div>
                        <div class="form-group">
                            <label>RT</label>
                            <select class="form-control" id="id_rt_ref" name="id_rt_ref">
                                <option value="">Pilih RT</option>
                            </select>
                            <div class="text-danger" id="_error_id_rt_ref"></div>
                        </div>
                    </div>
                    <!-- /col-6 -->
                </div>
                <!-- /row -->
            </div>
            <div class="modal-footer">
                <button title="Delete" class="btn btn-danger before_delete_wp"><i class="fa fa-trash"></i> Delete</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="bayar_wp_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Bayar SPPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('wp/bayar', 'id="form_bayar_wp"')?>
            <div class="modal-body">
                <input type="hidden" name="id_wp" id="_id_wp">
                <div class="form-group">
                    <label>Tanggal Bayar</label>
                    <input type="text" name="tgl_bayar" class="form-control datepicker" value="<?=date('d/m/Y')?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Bayar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="batal_bayar_wp_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Batal Bayar SPPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('wp/batal_bayar', 'id="form_batal_bayar_wp"')?>
            <div class="modal-body">
                <input type="hidden" name="id_wp" id="batal_id_wp">
                Apakah anda yakin ingin membatalkan pembayaran SPPT ini?
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Batal Bayar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delete_wp_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Delete SPPT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus SPPT ini?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn_delete_wp">Delete</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Import Karyawan -->
<div class="modal fade" id="import_wp_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Import SPPT</h4>
      </div>
      <?=form_open_multipart('wp/import', 'id="form_import_wp"')?>
      <div class="modal-body">
        <div class="alert alert-info">
          <strong>Note: </strong>
          <ul>
            <li>File yang diizinkan untuk diupload adalah file dengan ekstensi .xls/.xlsx</li>
            <li>Agar proses insert berjalan dengan benar, pastikan menggunakan template yang sudah disediakan</li>
            <li>Ukuran File Maksimal 2MB</li>
            <li>Pastikan Mengisi semua field.</li>
            <li>Maksimal input per satu aksi import adalah 20 baris, jika lebih dari itu maka baris 21 dst tidak di eksekusi</li>
          </ul>
        </div>
        <div class="form-group">
          <label>Excel File</label>
          <input type="file" name="userfile" class="form-control">
          <div class="text-danger" id="error_userfile"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Import</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->