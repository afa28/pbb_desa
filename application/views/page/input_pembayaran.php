<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Input Pembayaran</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">Input Pembayaran</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Input Pembayaran Pajak</strong>
                    </div>
                    <div class="card-body">
                        <?=form_open('wp/bayar', 'id="form_bayar_wp"')?>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Nama Rayon</label>
                                    <select class="form-control select2" name="id_rayon_ref" id="id_rayon_ref">
                                        <option value="">Pilih Rayon</option>
                                        <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                                            <?php foreach ($rayon as $key => $value): ?>
                                                <option value="<?=$value['id_rayon']?>"><?=$value['nama_rayon']?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                    <div class="text-danger" id="error_id_rayon_ref"></div>
                                </div>
                                <div class="form-group">
                                    <label>Nama Wajib Pajak</label>
                                    <select class="form-control select2" id="id_wp" name="id_wp">
                                        <option value="">Pilih Nama Wajib Pajak</option>
                                    </select>
                                    <div class="text-danger" id="error_id_wp"></div>
                                </div>
                                <div class="form-group">
                                    <label>Nomor Wajib Pajak</label>
                                    <input class="form-control" type="text" name="nomor_wp" id="nomor_wp" readonly="true"></input>
                                </div>
                                <div class="form-group">
                                    <label>Pagu Wajib Pajak</label>
                                    <input class="form-control" type="text" name="pagu_wp" id="pagu_wp" readonly="true"></input>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Bayar</label>
                                    <input class="form-control datepicker" type="text" name="tgl_bayar" id="tgl_bayar" value="<?=date('d/m/Y')?>"></input>
                                    <div class="text-danger" id="error_tgl_bayar"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="alert alert-sm alert-warning" id="alert_lunas" style="display: none;">WP tersebut telah lunas</div>
                                <button type="submit" class="btn btn-primary" id="btn_bayar">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->