<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Setting</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">Setting</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Informasi Dasar</strong>
                    </div>
                    <div class="card-body">
                        <?=form_open('setting/update', 'id="form_update_setting"')?>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kode Provinsi</label>
                                    <input type="text" class="form-control" name="kode_provinsi" value="<?=isset($setting['kode_provinsi']) ? $setting['kode_provinsi'] : ''?>" placeholder="Isikan Kode Provinsi">
                                    <div class="text-danger" id="error_kode_provinsi"></div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nama Provinsi</label>
                                    <input type="text" class="form-control" name="nama_provinsi" value="<?=isset($setting['nama_provinsi']) ? $setting['nama_provinsi'] : ''?>" placeholder="Isikan Nama Provinsi">
                                    <div class="text-danger" id="error_nama_provinsi"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kode Kabupaten</label>
                                    <input type="text" class="form-control" name="kode_kabupaten" value="<?=isset($setting['kode_kabupaten']) ? $setting['kode_kabupaten'] : ''?>" placeholder="Isikan Kode Kabupaten">
                                    <div class="text-danger" id="error_kode_kabupaten"></div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nama Kabupaten</label>
                                    <input type="text" class="form-control" name="nama_kabupaten" value="<?=isset($setting['nama_kabupaten']) ? $setting['nama_kabupaten'] : ''?>" placeholder="Isikan Nama Kabupaten">
                                    <div class="text-danger" id="error_nama_kabupaten"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kode Kecamatan</label>
                                    <input type="text" class="form-control" name="kode_kecamatan" value="<?=isset($setting['kode_kecamatan']) ? $setting['kode_kecamatan'] : ''?>" placeholder="Isikan Kode Kecamatan">
                                    <div class="text-danger" id="error_kode_kecamatan"></div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nama Kecamatan</label>
                                    <input type="text" class="form-control" name="nama_kecamatan" value="<?=isset($setting['nama_kecamatan']) ? $setting['nama_kecamatan'] : ''?>" placeholder="Isikan Nama Kecamatan">
                                    <div class="text-danger" id="error_nama_kecamatan"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kode Desa</label>
                                    <input type="text" class="form-control" name="kode_desa" value="<?=isset($setting['kode_desa']) ? $setting['kode_desa'] : ''?>" placeholder="Isikan Kode Desa">
                                    <div class="text-danger" id="error_kode_desa"></div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nama Desa</label>
                                    <input type="text" class="form-control" name="nama_desa" value="<?=isset($setting['nama_desa']) ? $setting['nama_desa'] : ''?>" placeholder="Isikan Nama Desa">
                                    <div class="text-danger" id="error_nama_desa"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Logo Surat</label>
                                    <input type="file" class="form-control" name="logo_surat">
                                    <div class="text-danger" id="error_logo_surat"></div>

                                    <div id="logo_surat" class="mt-3">
                                        <?php if (isset($setting['logo_surat'])): ?>
                                            <?php if (file_exists(FCPATH.'assets/logo/'.$setting['logo_surat'])): ?>
                                                <img src="<?=base_url()?>assets/logo/<?=$setting['logo_surat']?>">
                                            <?php endif ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->