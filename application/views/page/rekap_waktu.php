<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Rekap Waktu</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">Rekap Waktu</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Rekap Waktu</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table_setor_bank" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Pelunasan</th>
                                        <th>Jumlah Penerimaan</th>
                                        <th>Status Setor</th>
                                        <th>Tanggal Setor</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="detail_setor_bank_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Data SPPT Tanggungan Rekap Waktu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <input type="hidden" id="tgl_bayar">
                    <table id="table_detail_setor_bank" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>RT</th>
                                <th>Total Pajak</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="setor_bank_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Setor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('setor_bank/setor', 'id="form_setor_bank"')?>
            <div class="modal-body">
                <input type="hidden" name="tgl_bayar" id="tgl_bayar_setor">
                <div class="form-group">
                    <label>Tanggal Setor</label>
                    <input type="text" name="tgl_setor" class="form-control datepicker" value="<?=date('d/m/Y')?>">
                    <div class="text-danger" id="error_tgl_setor"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Setor</button>
            </div>
            </form>
        </div>
    </div>
</div>