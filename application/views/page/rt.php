<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>RT</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?=base_url()?>">Dashboard</a></li>
                            <li class="active">RT</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data RT</strong>
                    </div>
                    <div class="card-body">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#add_rt_modal"><i class="fa fa-plus-circle"></i> Tambah</button>
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="table_rt" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama RT</th>
                                        <th>Nama Rayon</th>
                                        <th>Total Pagu</th>
                                        <th>Total Bayar</th>
                                        <th>Kekurangan</th>
                                        <th>Pagu Terpenuhi</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<div class="modal fade" id="detail_rt_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Data SPPT Tanggungan RT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <input type="hidden" id="id_rt_">
                    <table id="table_detail_rt" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>RT</th>
                                <th>Total Pajak</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_rt_modal" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Tambah RT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('rt/add', 'id="form_add_rt"')?>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama RT</label>
                    <input class="form-control" type="text" name="nama_rt" placeholder="Nama RT">
                    <div class="text-danger" id="error_nama_rt"></div>
                </div>
                <div class="form-group">
                    <label>Rayon</label>
                    <select class="form-control select2" name="id_rayon_ref">
                        <option value="">Pilih Rayon</option>
                        <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                            <?php foreach ($rayon as $key => $value): ?>
                                <option value="<?=$value['id_rayon']?>"><?=$value['nama_rayon']?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                    <div class="text-danger" id="error_id_rayon_ref"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_rt_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Edit RT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('rt/update', 'id="form_update_rt"')?>
            <div class="modal-body">
                <input type="hidden" name="id_rt" id="id_rt">
                <div class="form-group">
                    <label>Nama RT</label>
                    <input class="form-control" type="text" id="nama_rt" name="nama_rt" placeholder="Nama RT">
                    <div class="text-danger" id="_error_nama_rt"></div>
                    <div class="form-group">
                    <label>Rayon</label>
                    <select class="form-control" name="id_rayon_ref" id="id_rayon_ref">
                        <option value="">Pilih Rayon</option>
                        <?php if (isset($rayon) && is_array($rayon) && count($rayon) > 0): ?>
                            <?php foreach ($rayon as $key => $value): ?>
                                <option value="<?=$value['id_rayon']?>"><?=$value['nama_rayon']?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                    <div class="text-danger" id="_error_id_rayon_ref"></div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delete_rt_modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Delete RT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_open('rt/delete', 'id="form_delete_rt"')?>
            <input type="hidden" name="id_rt" id="delete_id_rt">
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus RT ini?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
            </form>
        </div>
    </div>
</div>