<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pencatatan Pajak | <?=isset($title) ? $title : 'Dashboard'?></title>
    <meta name="description" content="Syaqila & Quensha Menjual berbagai jenis hijab dan gamis">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/normalize.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/loading_modal.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/daterangepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/accordion.css">
    
   <style>
    .modal-title {
        float: left;
    }
    </style>
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=base_url()?>"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li class="menu-title">Main Menu</li>
                    <li>
                        <a href="<?=base_url()?>wp"><i class="menu-icon fa fa-cubes"></i> SPPT</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>rayon"><i class="menu-icon fa fa-user"></i>Rayon </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>rt"><i class="menu-icon fa fa-home"></i>RT </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>users"><i class="menu-icon fa fa-users"></i>Users </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>setting"><i class="menu-icon fa fa-gear"></i>Setting </a>
                    </li>
                    <li class="menu-title">Transaksi</li>
                    <li>
                        <a href="<?=base_url()?>input_pembayaran"><i class="menu-icon fa fa-money"></i>Input Pembayaran </a>
                    </li>
                    <!-- <li>
                        <a href="<?=base_url()?>setor_bank"><i class="menu-icon fa fa-bank"></i>Setor Bank </a>
                    </li> -->
                    <li class="menu-title">Archive</li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-archive"></i>Rekap Pembayaran</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-clock"></i><a href="<?=base_url()?>rekap_waktu">Rekap Waktu</a></li>
                            <li><i class="menu-icon fa fa-times"></i><a href="<?=base_url()?>rekap_belum_bayar">Rekap Belum Bayar</a></li>
                            <li><i class="menu-icon fa fa-check"></i><a href="<?=base_url()?>rekap_lunas">Rekap Lunas</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?=base_url()?>rekap"><i class="menu-icon fa fa-archive"></i>Cetak Rekap</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="./"><img src="<?=base_url()?>assets/images/logo.png" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="<?=base_url()?>assets/images/logo2.png" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">
                    </div>

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span><?=isset($login_username) ? $login_username : 'Admin'?></span> &nbsp;&nbsp;&nbsp;<img class="user-avatar rounded-circle" src="<?=base_url()?>assets/images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?=base_url()?>change_password"><i class="fa fa- user"></i>Change Password</a>
                            <a class="nav-link" href="<?=base_url()?>login/logout"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>
        <!-- /#header -->