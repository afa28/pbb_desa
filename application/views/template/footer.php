    <!-- Modal Loading -->
    <div data-backdrop="static" data-keyboard="false" class="modal fade" id="loading" tabindex="1" role="dialog" aria-labelledby="loadMeLabel" aria-hidden="true" style="top:0;padding-top: 160px">
      <div class="modal-dialog modal-sm" role="document" style="vertical-align: middle !important;">
        <div class="modal-content">
          <div class="modal-body text-center">
            <div class="loader"></div>
            <div clas="loader-txt">
              <p>Mohon Tunggu Sebentar ...</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <!-- Footer -->
    <footer class="site-footer">
        <div class="footer-inner bg-white">
            <div class="row">
                <div class="col-sm-6">
                    Copyright &copy; 2018 Ela Admin
                </div>
                <div class="col-sm-6 text-right">
                    Designed by <a href="https://colorlib.com">Colorlib</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- /.site-footer -->
</div>
<!-- /#right-panel -->

<!-- Scripts -->
<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
<script src="<?=base_url()?>assets/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.matchHeight.min.js"></script>
<script src="<?=base_url()?>assets/js/main.js"></script>

<script src="<?=base_url()?>assets/js/moment.min.js"></script>
<script src="<?=base_url()?>assets/js/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

<script type="text/javascript">
    /** add active class and stay opened when selected */
    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.navbar-nav a').filter(function() {
    return this.href == url;
    }).parent().addClass('active');

    // for treeview
    $('ul.dropdown-menu a').filter(function() {
    return this.href == url;
    }).parent().parent().parent().addClass('active show').attr('aria-expanded', true).children().addClass('show');

    const base_url = '<?=base_url()?>';
</script>

<?php if (isset($page) && $page == 'change_password'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/change_password.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'users'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/users.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'wp'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/wp.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'rayon'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/rayon.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'rt'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/rt.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'setor_bank'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/setor_bank.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'rekap'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/rekap.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'setting'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/setting.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'input_pembayaran'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/input_pembayaran.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'rekap_waktu'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/rekap_waktu.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'rekap_belum_bayar'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/rekap_belum_bayar.js"></script>
<?php endif ?>

<?php if (isset($page) && $page == 'rekap_lunas'): ?>
    <script type="text/javascript" src="<?=base_url()?>assets/js/rekap_lunas.js"></script>
<?php endif ?>

</body>
</html>
