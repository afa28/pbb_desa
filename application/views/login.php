<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pemungutan Pajak | Login</title>
    <meta name="description" content="Syaqila & Quensha Menjual berbagai jenis hijab dan gamis">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="<?=base_url()?>assets/css/normalize.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/loading_modal.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body class="bg-dark">

    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="<?=base_url()?>assets/images/logo.png" alt="">
                    </a>
                </div>
                <div class="login-form">
                    <div id="notif" class="alert" style="display: none;"></div>
                    <?=form_open('login', 'id="form_login"')?>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" name="login_username" placeholder="Username">
                            <div class="text-danger" id="error_login_username"></div>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="login_password" placeholder="Password">
                            <div class="text-danger" id="error_login_password"></div>
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Loading -->
    <div data-backdrop="static" data-keyboard="false" class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel" aria-hidden="true" style="top:0;padding-top: 160px">
      <div class="modal-dialog modal-sm" role="document" style="vertical-align: middle !important;">
        <div class="modal-content">
          <div class="modal-body text-center">
            <div class="loader"></div>
            <div clas="loader-txt">
              <p>Mohon Tunggu Sebentar ...</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        const base_url = '<?=base_url()?>';
    </script>
    <script src="<?=base_url()?>assets/js/login.js"></script>

</body>
</html>
