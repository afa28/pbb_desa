<!-- <img src="<?php echo $_SERVER['DOCUMENT_ROOT'].'/feri/assets/image/download.png' ?>" alt=""> -->

<style>
#tblData{
  width:100%; 
  margin-top: 150px; 
  border: 1px solid black;
  border-collapse: collapse;
}

#tblData tr{
  border: 1px solid black;
}

#tblData td{
  font-size: 12px; 
  width: 30px;
  border: 1px solid black;
}

#spacing{
  border: 1px solid white;
}
</style>

<?php if (isset($data) && is_array($data)): ?>
  <?php 

    $count_data = count($data); 
    $page = $count_data / 15;
    if ($page < 1) {
      $page = 1;
    }

    $chunk = array_chunk($data, 2);
  ?>
<?php endif ?>

<?php for ($i=0; $i < $page; $i++): ?>
  <?php if ($i > 0): ?>
    <div style="page-break-before: always;"></div>
  <?php endif ?>
  <table style="width:45%; float: left;">
    <tr>
      <td style="width: 100px; font-family: helvetica;"><img src="<?=isset($setting['logo_surat']) ? 'assets/logo/'.$setting['logo_surat'] : 'assets/images/logo-kabupaten.png'?>" alt="" width="100"></td>
      <td valign="top" align="center" style="width: 350px; font-family: helvetica;">
          <b>DINAS PENDAPATAN DAERAH</b><br>
          <div style="font-size: 12px;"> (DISPENDA) <br>
          KABUPATEN <?=isset($setting['nama_kabupaten']) ? $setting['nama_kabupaten'] : 'PARIGI MOUTONG'?> <br>
          <?=isset($setting['nama_provinsi']) ? $setting['nama_provinsi'] : 'SULAWESI TENGAH'?> </div>
          <br>
          <div style="font-size: 14px; font-family: helvetica;">
              <b>PELAYANAN PAJAK DAERAH <br>
              PAJAK BUMI DAN BANGUNAN (PBB-P2)</b>
          </div>
      </td>
    </tr>
  </table>

  <table style="width:45%; float: left;">
    <tr style="height: 250px;">
      <td valign="top" style="font-family: helvetica;" colspan="5">
          <b>DAFTAR PENERIMAAN HARIAN PAJAK BUMI DAN BANGUNAN</b>
      </td>
    </tr>
    <tr style="line-height: 28px;">
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="font-size: 12px; width: 100px;">PROVINSI</td>
      <td style="font-size: 12px; width: 6px;">:</td>
      <td style="font-size: 12px; width: 10px;">(<?=isset($setting['kode_provinsi']) ? $setting['kode_provinsi'] : '72'?>)</td>
      <td style="font-size: 12px;"><?=isset($setting['nama_provinsi']) ? $setting['nama_provinsi'] : 'SULAWESI TENGAH'?></td>
    </tr>
    <tr>
      <td style="font-size: 12px;">KABUPATEN</td>
      <td style="font-size: 12px;">:</td>
      <td style="font-size: 12px;">(<?=isset($setting['kode_kabupaten']) ? $setting['kode_kabupaten'] : '08'?>)</td>
      <td style="font-size: 12px;"><?=isset($setting['nama_kabupaten']) ? $setting['nama_kabupaten'] : 'PARIGI MOUTONG'?></td>
    </tr>
    <tr>
      <td style="font-size: 12px;">KECAMATAN</td>
      <td style="font-size: 12px;">:</td>
      <td style="font-size: 12px;">(<?=isset($setting['kode_kecamatan']) ? $setting['kode_kecamatan'] : '14'?>)</td>
      <td style="font-size: 12px;"><?=isset($setting['nama_kecamatan']) ? $setting['nama_kecamatan'] : 'TAOPA'?></td>
    </tr>
    <tr>
      <td style="font-size: 12px;">DESA/KELURAHAN</td>
      <td style="font-size: 12px;">:</td>
      <td style="font-size: 12px;">(<?=isset($setting['kode_desa']) ? $setting['kode_desa'] : '2008'?>)</td>
      <td style="font-size: 12px;"><?=isset($setting['nama_desa']) ? $setting['nama_desa'] : 'TOMPO'?></td>
    </tr>
  </table>

  <table style="width:10%; float: left; margin-top: 50px; border: 1px solid black;">
    <tr align="center">
      <td style="font-size: 12px;">
          Lembar <?=$i+1?>
      </td>
    </tr>
    <tr align="center">
      <td style="border: 1px solid black; font-size: 12px;">
          Untuk Desa / <br>
          Kelurahan
      </td>
    </tr>
  </table>

  <table id="tblData">
    <tr>
      <td rowspan="3" align="center">No.</td>
      <td rowspan="3" align="center" >
        NOMOR BLOCK, <br>
        NOMOR URUT OP, <br>
        KODE OP
      </td>
      <td rowspan="3" align="center">
        TAHUN
      </td>
      <td colspan="5" align="center">
        JUMLAH PEMBAYARAN RUPIAH
      </td>
      <td rowspan="3" align="center" align="center">Jumlah <br> (Rp)</td>
      <td rowspan="3" align="center" align="center">STTS</td>
    </tr>
    <tr>
      <td colspan="2" align="center">PAJAK TERHUTANG PADA</td>
      <td colspan="3" align="center">SURAT TAGIHAN PAJAK</td>
    </tr>
    <tr>
      <td align="center">SPPT</td>
      <td align="center">SKP</td>
      <td align="center">Pajak</td>
      <td align="center">Denda</td>
      <td align="center">Jumlah</td>
    </tr>
    <tr>
      <td align="center">1</td>
      <td align="center">2</td>
      <td align="center">3</td>
      <td align="center">4</td>
      <td align="center">5</td>
      <td align="center">6</td>
      <td align="center">7</td>
      <td align="center">8(6+7)</td>
      <td align="center">9</td>
      <td align="center">10</td>
    </tr> 

    <?php if (isset($chunk[$i]) && is_array($chunk[$i]) && count($chunk[$i]) > 0): ?>
      <?php $total = 0; ?>
      <?php foreach ($chunk[$i] as $key => $value) : ?>
        <?php $total +=$value['pagu_wp']; ?>
        <tr>
          <td align="center"><?=$key+1?></td>
          <td align="center"><?=$value['nomor_wp']?></td>
          <td align="center"><?=(($value['tgl_bayar'] != NULL) ? date('Y', strtotime($value['tgl_bayar'])) : date('Y'))?></td>
          <td align="center">Rp. <?=rupiah($value['pagu_wp'])?></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="center">Rp <?=rupiah($value['pagu_wp'])?></td>
          <td align="center"><?=(($value['status'] == 2) ? 'Lunas' : 'Terhutang')?></td>
        </tr>
      <?php endforeach;?>

      <?php if ($jumlah_data = count($chunk[$i]) < 15): ?>
        <?php for($a = $jumlah_data+1; $a <= 15; $a++): ?>
          <tr>
          <td align="center"><?=$a?></td>
          <td align="center"></td>
          <td align="center"></td>
          <td align="center"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="center"></td>
          <td></td>
        </tr>
        <?php endfor; ?>
      <?php endif ?>
    <?php endif;?>
  </table>

  <table style="width:100%; 
    margin-top: 25px; 
    border-collapse: collapse;">
    <tr>
      <td align="center" style="font-size: 12px; width: 31%; border: 1px solid black;"><b>Jumlah</b></td>
      <td align="center" style="font-size: 12px; border: 1px solid black; width: 10%">&nbsp;</td>
      <td align="center" style="font-size: 12px; border: 1px solid black; width: 8%">Rp <?=isset($total) ? rupiah($total) : 0?></td>
      <td style="font-size: 12px; border: 1px solid black;width: 9%">&nbsp;</td>
      <td style="font-size: 12px; border: 1px solid black; width: 7%">&nbsp;</td>
      <td style="font-size: 12px; border: 1px solid black;width: 8%">&nbsp;</td>
      <td style="font-size: 12px; border: 1px solid black;width: 8%">&nbsp;</td>
      <td align="center" style="font-size: 12px; border: 1px solid black; width: 8%">Rp <?=isset($total) ? rupiah($total) : 0?></td>
      <td align="center" style="font-size: 12px; border: 1px solid black; ">&nbsp;</td>
    </tr>
  </table>

  <table style="width:100%; margin-top: 25px; border-collapse: collapse;">
    <tr style="border: 1px solid black;">
      <td style="font-size: 12px; width: 190px; border: 1px solid black;" align="center">Jumlah Setoran Telah Diterima</td>
      <td style="font-size: 12px; width: 110px; border: 1px solid black;" align="center">Tanda Tangan dan <br> Cap Bank</td>
      <td style="width: 340px;">&nbsp;</td>
      <td style="font-size: 12px; width: 140px;" align="center" valign="bottom">Petugas Pemungut</td>
      <td style="width: 100px;">&nbsp;</td>
    </tr>
    <tr>
      <td style="font-size: 12px; width: 40px; border: 1px solid black; padding: 8px;">
        Tanggal <br>
        ................................................................ <br>
        Nama Bank <br>
        ................................................................
      </td>
      <td style="font-size: 12px; width: 40px; border: 1px solid black;">
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td style="font-size: 12px;" align="center" valign="bottom">
        <b><?=isset($nama_rayon) ? $nama_rayon : ''?></b>
      </td>
      <td style="font-size: 12px;" align="center" valign="bottom">
        Hal: &nbsp; <?=$i+1?>
      </td>
    </tr>
  </table>
<?php endfor; ?>