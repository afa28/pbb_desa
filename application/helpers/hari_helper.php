<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function tanggal_indonesia($format, $date)
{
	if ($format == 'd M Y') {
		$tanggal = explode(' ', $date);
		if ($tanggal[1] == 'Jan') {
			$tanggal[1] = 'Januari';
		}elseif ($tanggal[1] == 'Feb') {
			$tanggal[1] = 'Februari';
		}elseif ($tanggal[1] == 'Mar') {
			$tanggal[1] = 'Maret';
		}elseif ($tanggal[1] == 'Apr') {
			$tanggal[1] = 'April';
		}elseif ($tanggal[1] == 'May') {
			$tanggal[1] = 'Mei';
		}elseif ($tanggal[1] == 'Jun') {
			$tanggal[1] = 'Juni';
		}elseif ($tanggal[1] == 'Jul') {
			$tanggal[1] = 'Juli';
		}elseif ($tanggal[1] == 'Aug') {
			$tanggal[1] = 'Agusus';
		}elseif ($tanggal[1] == 'Sep') {
			$tanggal[1] = 'September';
		}elseif ($tanggal[1] == 'Oct') {
			$tanggal[1] = 'Oktober';
		}elseif ($tanggal[1] == 'Nov') {
			$tanggal[1] = 'November';
		}else{
			$tanggal[1] = 'Desember';
		}

		$tgl = $tanggal[0].' '.$tanggal[1].' '.$tanggal[2];
		return $tgl;
	}elseif($format == 'D, d M Y'){
		$section = explode(',', $date);
		$tanggal = explode(' ', $section[1]);

		$day = $section[0];

		if ($day == 'Mon') {
			$day = 'Senin';
		}elseif ($day == 'Tue') {
			$day = 'Selasa';
		}elseif ($day == 'Wed') {
			$day = 'Rabu';
		}elseif ($day == 'Thu') {
			$day = 'Kamis';
		}elseif ($day == 'Fri') {
			$day = 'Jumat';
		}elseif ($day == 'Sat') {
			$day = 'Sabtu';
		}else{
			$day = 'Minggu';
		}

		if ($tanggal[2] == 'Jan') {
			$tanggal[2] = 'Januari';
		}elseif ($tanggal[2] == 'Feb') {
			$tanggal[2] = 'Februari';
		}elseif ($tanggal[2] == 'Mar') {
			$tanggal[2] = 'Maret';
		}elseif ($tanggal[2] == 'Apr') {
			$tanggal[2] = 'April';
		}elseif ($tanggal[2] == 'May') {
			$tanggal[2] = 'Mei';
		}elseif ($tanggal[2] == 'Jun') {
			$tanggal[2] = 'Juni';
		}elseif ($tanggal[2] == 'Jul') {
			$tanggal[2] = 'Juli';
		}elseif ($tanggal[2] == 'Aug') {
			$tanggal[2] = 'Agusus';
		}elseif ($tanggal[2] == 'Sep') {
			$tanggal[2] = 'September';
		}elseif ($tanggal[2] == 'Oct') {
			$tanggal[2] = 'Oktober';
		}elseif ($tanggal[2] == 'Nov') {
			$tanggal[2] = 'November';
		}else{
			$tanggal[2] = 'Desember';
		}

		return $day.', '.$tanggal[1].' '.$tanggal[2].' '.$tanggal[3];

	}else{
		$tgl = "format doesn't exist";
		return $tgl;
	}
}

if (!function_exists('tgl_indo')) {
	function tgl_indo($date)
	{
		$tanggal = explode('-', $date);
		if ($tanggal[1] == 01) {
			$tanggal[1] = 'Januari';
		}elseif ($tanggal[1] == '02') {
			$tanggal[1] = 'Februari';
		}elseif ($tanggal[1] == '03') {
			$tanggal[1] = 'Maret';
		}elseif ($tanggal[1] == '04') {
			$tanggal[1] = 'April';
		}elseif ($tanggal[1] == '05') {
			$tanggal[1] = 'Mei';
		}elseif ($tanggal[1] == '06') {
			$tanggal[1] = 'Juni';
		}elseif ($tanggal[1] == '07') {
			$tanggal[1] = 'Juli';
		}elseif ($tanggal[1] == '08') {
			$tanggal[1] = 'Agusus';
		}elseif ($tanggal[1] == '09') {
			$tanggal[1] = 'September';
		}elseif ($tanggal[1] == '10') {
			$tanggal[1] = 'Oktober';
		}elseif ($tanggal[1] == '11') {
			$tanggal[1] = 'November';
		}else{
			$tanggal[1] = 'Desember';
		}

		return $tanggal[2].' '.$tanggal[1].' '.$tanggal[0];
	}
}

?>