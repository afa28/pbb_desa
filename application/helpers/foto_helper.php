<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('generate_profile')) {
	function generate_profile($link)
	{
		$ci =&get_instance();

		$config['image_library'] = 'gd2';
		$config['source_image'] = $link;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 512;

		$ci->load->library('image_lib', $config);

		$ci->image_lib->resize();
		$ci->image_lib->clear();

		$config2['image_library'] = 'gd2';
		$config2['source_image'] = $link;
		$config2['create_thumb'] = FALSE;
		$config2['maintain_ratio'] = FALSE;
		$config2['width']         = 512;
		$config2['height']         = 512;

		$ci->image_lib->initialize($config2);

		$ci->image_lib->crop();
	}
}