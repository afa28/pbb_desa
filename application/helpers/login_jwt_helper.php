<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Cek Login
function cek_login()
{
	if (isset($_COOKIE['user_logged_in'])) {
		$channel = curl_init();
		curl_setopt($channel, CURLOPT_HTTPHEADER, array(
		    'API-KEY: pajakapikey123'
		));
		curl_setopt( $channel, CURLOPT_URL, HOST_SERVER.'login_api?jwt_key='.$_COOKIE['user_logged_in']);
		curl_setopt( $channel, CURLOPT_RETURNTRANSFER, 1 ); 
		$response= json_decode(curl_exec ( $channel ), true);
		curl_close ( $channel );
		return $response;
	}else{
		return "NoData";
	}
}

//Data Login
function data_login($index)
{
	if (isset($_COOKIE['user_logged_in'])) {
		$channel = curl_init();
		curl_setopt($channel, CURLOPT_HTTPHEADER, array(
		    'API-KEY: pajakapikey123'
		));
		curl_setopt( $channel, CURLOPT_URL, HOST_SERVER.'login_api?jwt_key='.$_COOKIE['user_logged_in']);
		curl_setopt( $channel, CURLOPT_RETURNTRANSFER, 1 ); 
		$response= json_decode(curl_exec ( $channel ), true);
		curl_close ( $channel );
		$res = ((isset($response['data'][$index])) ? $response['data'][$index] : "NoData");
	}else{
		$res = "NoData";
	}
	if ($res == "NoData") {
		redirect('login/logout');
	}else{
		return $res;
	}
}