<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Email Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/email_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('send_mail'))
{
	/**
	 * Validate email address
	 *
	 * @deprecated	3.0.0	Use PHP's filter_var() instead
	 * @param	string	$email
	 * @return	bool
	 */
	function send_mail($data=array(""))
	{	
		require __DIR__ . '/vendor/autoload.php';

		$email = new \SendGrid\Mail\Mail(); 
		$email->setFrom(EMAIL_PENGIRIM, NAMA_PENGIRIM);
		$email->setSubject($data['subject']);
		$email->addTo($data['to']);
		$email->addContent(
		    "text/html", $data['message']
		);
		if (isset($data['attach'])) {
			$file_encoded = $data['attach']['file'];
			$file_title = $data['attach']['title'];
			$file_format = $data['attach']['format'];
			
			$email->addAttachment(
			    $file_encoded,
			    $file_format,
			    $file_title,
			    "attachment"
			);
		}
		$sendgrid = new \SendGrid(SENDGRID_API_KEY);
		try {
		    $response = $sendgrid->send($email);
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		}
	}
		
}

?>