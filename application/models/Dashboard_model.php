<?php 
/**
 * 
 */
class Dashboard_model extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	public function get_total_sppt()
	{
		return $this->db->get('tb_wp')->num_rows();
	}

	public function get_total_rayon()
	{
		return $this->db->get('tb_rayon')->num_rows();
	}

	public function get_total_rt()
	{
		return $this->db->get('tb_rt')->num_rows();
	}

	public function get_total_pagu()
	{
		$query = "SELECT SUM(pagu_wp) as total_pagu FROM tb_wp";
		return $this->db->query($query)->row_array()['total_pagu'];
	}

	public function get_pagu_terhutang()
	{
		$query = "SELECT SUM(pagu_wp) as total_pagu FROM tb_wp WHERE status = ?";
		return $this->db->query($query, 1)->row_array()['total_pagu'];
	}

	public function get_pagu_terbayar()
	{
		$query = "SELECT SUM(pagu_wp) as total_pagu FROM tb_wp WHERE status = ?";
		return $this->db->query($query, 2)->row_array()['total_pagu'];
	}

	public function get_total_sppt_terhutang()
	{
		$this->db->where('status', 1);
		return $this->db->get('tb_wp')->num_rows();
	}

	public function get_total_sppt_terbayar()
	{
		$this->db->where('status', 2);
		return $this->db->get('tb_wp')->num_rows();
	}

	public function get_riwayat_pembayaran($limit = 4)
	{
		$query = "SELECT wp.nama_wp, wp.pagu_wp, b.tgl_bayar 
					FROM tb_bayar b 
					JOIN tb_wp wp ON b.id_wp_ref=wp.id_wp 
					LIMIT {$limit}";
		return $this->db->query($query)->result_array();
	}

	public function get_rayon()
	{
		$query = "SELECT r.id_rayon, r.nama_rayon, SUM(wp.pagu_wp) as total_pagu, COUNT(wp.id_wp) as jumlah_wp FROM tb_rayon r JOIN tb_rt rt ON r.id_rayon=rt.id_rayon_ref JOIN tb_wp wp ON rt.id_rt=wp.id_rt_ref GROUP BY r.id_rayon LIMIT 8";
		return $this->db->query($query)->result_array();
	}
}

 ?>