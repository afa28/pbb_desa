<?php 
/**
 * 
 */
class Rt_model extends CI_Model
{
	
	var $table = 'tb_rt rt';
	var $column_order = array(null, 'rt.nama_rt','r.nama_rayon',null,null,null,null,null);
	var $column_search = array('rt.nama_rt','r.nama_rayon');
	var $order = array('rt.id_rt' => 'DESC');

	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	private function _get_datatables_query()
	{	
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('tb_rayon r', 'rt.id_rayon_ref=r.id_rayon', 'left');
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_rt_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{	
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function add_rt($data)
	{
		$this->db->insert('tb_rt', $data);
	}

	public function update_rt($data, $id_rt)
	{
		$this->db->where('id_rt', $id_rt);
		$this->db->update('tb_rt', $data);
	}

	public function get_detail_rt($id_rt){
		$this->db->where('id_rt', $id_rt);
		return $this->db->get('tb_rt')->row_array();
	}

	public function get_rayon_option(){
		return $this->db->get('tb_rayon')->result_array();
	}

	public function delete_rt($id_rt)
	{
		$this->db->where('id_rt', $id_rt);
		$this->db->delete('tb_rt');
	}
}
 ?>