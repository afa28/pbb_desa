<?php 
/**
 * 
 */
class Rayon_model extends CI_Model
{
	var $table = 'tb_rayon';
	var $column_order = array(null, 'nama_rayon',null,null,null,null,null);
	var $column_search = array('nama_rayon');
	var $order = array('id_rayon' => 'DESC');

	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	private function _get_datatables_query()
	{	
		$this->db->select('*');
		$this->db->from($this->table);

		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_rayon_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{	
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function add_rayon($data)
	{
		$this->db->insert('tb_rayon', $data);
	}

	public function update_rayon($data, $id_rayon)
	{
		$this->db->where('id_rayon', $id_rayon);
		$this->db->update('tb_rayon', $data);
	}

	public function get_detail_rayon($id_rayon){
		$this->db->where('id_rayon', $id_rayon);
		return $this->db->get('tb_rayon')->row_array();
	}

	public function delete_rayon($id_rayon)
	{
		$this->db->where('id_rayon', $id_rayon);
		$this->db->delete('tb_rayon');
	}

	public function get_total_pagu_per_rayon($id_rayon_ref)
	{
		$query_pagu = "SELECT sum(w.pagu_wp) as total_pagu FROM tb_wp w
						JOIN tb_rt rt ON rt.id_rt=w.id_rt_ref
						WHERE rt.id_rayon_ref = ?";
		return $this->db->query($query_pagu, array($id_rayon_ref))->row_array()['total_pagu'];
	}

	public function get_total_bayar_per_rayon($id_rayon_ref)
	{
		$query_bayar = "SELECT sum(w.pagu_wp) as total_bayar FROM tb_wp w
						JOIN tb_rt rt ON rt.id_rt=w.id_rt_ref
						WHERE rt.id_rayon_ref = ? AND w.status = ?";
		return $this->db->query($query_bayar, array($id_rayon_ref, 2))->row_array()['total_bayar'];
	}

	public function get_riwayat_pembayaran($id_rayon_ref)
	{
		$query = "SELECT wp.nama_wp, b.tgl_bayar 
					FROM tb_bayar b 
					JOIN tb_wp wp ON b.id_wp_ref=wp.id_wp 
					JOIN tb_rt rt ON wp.id_rt_ref=rt.id_rt
					WHERE rt.id_rayon_ref = ?
					LIMIT 5";
		return $this->db->query($query, array($id_rayon_ref))->result_array();
	}
}
 ?>