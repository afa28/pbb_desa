<?php 
/**
 * 
 */
class Login_model extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	public function get_data_login($login_username, $login_password)
	{
		$query = "SELECT id_login, status FROM tb_login WHERE login_username = ? AND login_password = ?";
		return $this->db->query($query, array($login_username, $login_password))->row_array();
	}
}
 ?>