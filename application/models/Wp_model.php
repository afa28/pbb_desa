<?php 
/**
 * 
 */
class Wp_model extends CI_Model
{
	
	var $table = 'tb_wp w';
	var $column_order = array(null, 'w.nomor_wp','w.nama_wp','r.nama_rayon','rt.nama_rt', 'w.pagu_wp','w.status', null);
	var $column_search = array('w.nomor_wp','w.nama_wp','r.nama_rayon','rt.nama_rt', 'w.pagu_wp','w.status');
	var $order = array('id_wp' => 'DESC');

	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	private function _get_datatables_query()
	{	
		$this->db->select('w.*, r.nama_rayon, rt.nama_rt');
		$this->db->from($this->table);
		$this->db->join('tb_rt rt', 'w.id_rt_ref=rt.id_rt','left');
		$this->db->join('tb_rayon r', 'r.id_rayon=rt.id_rayon_ref','left');

		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_wp_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{	
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function add_wp($data)
	{
		$this->db->insert('tb_wp', $data);
	}

	public function update_wp($data, $id_wp)
	{
		$this->db->where('id_wp', $id_wp);
		$this->db->update('tb_wp', $data);
	}

	public function get_detail_wp($id_wp){
		$query = "SELECT w.*, rt.id_rayon_ref as id_rayon FROM tb_wp w
					JOIN tb_rt rt ON w.id_rt_ref=rt.id_rt
					WHERE w.id_wp = ?";
		return $this->db->query($query, array($id_wp))->row_array();
	}

	public function get_rayon_option()
	{
		return $this->db->get('tb_rayon')->result_array();
	}

	public function get_rt_option($id_rayon = NULL)
	{
		if ($id_rayon != NULL) {
			$this->db->where('id_rayon_ref', $id_rayon);
		}
		return $this->db->get('tb_rt')->result_array();
	}

	public function delete_wp($id_wp)
	{
		$this->db->where('id_wp', $id_wp);
		$this->db->delete('tb_wp');
	}

	public function add_bayar($data)
	{
		$this->db->insert('tb_bayar', $data);
	}

	public function delete_bayar($id_wp)
	{
		$this->db->where('id_wp_ref', $id_wp);
		$this->db->delete('tb_bayar');
	}

	public function get_count_rt()
	{
		return $this->db->get('tb_rt')->num_rows();
	}

	public function get_wp_option($id_rayon_ref)
	{
		$query = "SELECT wp.id_wp, wp.nomor_wp, wp.nama_wp FROM tb_wp wp
				JOIN tb_rt rt ON wp.id_rt_ref=rt.id_rt
				WHERE rt.id_rayon_ref = ?";
		return $this->db->query($query, array($id_rayon_ref))->result_array();
	}
}
 ?>