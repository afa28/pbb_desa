<?php 
/**
 * 
 */
class Rekap_model extends CI_Model
{
	var $table = 'tb_wp w';
	var $column_order = array(null, 'w.nomor_wp','w.nama_wp','r.nama_rayon','rt.nama_rt', 'w.pagu_wp','w.status');
	var $column_search = array('w.nomor_wp','w.nama_wp','r.nama_rayon','rt.nama_rt', 'w.pagu_wp','w.status');
	var $order = array('id_wp' => 'DESC');

	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	private function _get_datatables_query()
	{	
		$tipe_rekap = $this->input->post('tipe_rekap');
		$id_rayon_ref = $this->input->post('id_rayon_ref');
		$id_rt_ref = $this->input->post('id_rt_ref');
		$bulan_bayar = $this->input->post('bulan_bayar') != '' ? $this->input->post('bulan_bayar') : NULL;
		$tgl_bayar = $this->input->post('tgl_bayar') != '' ? $this->input->post('tgl_bayar') : NULL;
		if ($tgl_bayar != NULL) {
			$tgl = explode(' - ' ,$this->input->post('tgl_bayar'));
			$start_bayar = date('Y-m-d', strtotime($tgl[0]));
			$end_bayar = date('Y-m-d', strtotime($tgl[1]));
		}

		$tgl_setor = $this->input->post('tgl_setor') != '' ? $this->input->post('tgl_setor') : NULL;
		if ($tgl_setor != NULL) {
			$tgl = explode(' - ' ,$this->input->post('tgl_setor'));
			$start_setor = date('Y-m-d', strtotime($tgl[0]));
			$end_setor = date('Y-m-d', strtotime($tgl[1]));
		}

		if ($tipe_rekap == 'filter_penerimaan_rayon') {
			$this->db->where('r.id_rayon', $id_rayon_ref);
			$this->db->where('rt.id_rt', $id_rt_ref);
		}elseif ($tipe_rekap == 'filter_bulan_rayon') {
			$this->db->where('r.id_rayon', $id_rayon_ref);
			$this->db->where('MONTH(b.tgl_bayar)', $bulan_bayar);
			$this->db->where('YEAR(b.tgl_bayar)', date('Y'));
		}elseif ($tipe_rekap == 'filter_tgl_rayon') {
			$this->db->where('r.id_rayon', $id_rayon_ref);
			$this->db->where('b.tgl_bayar >=', $start_bayar);
			$this->db->where('b.tgl_bayar <=', $end_bayar);
		}elseif ($tipe_rekap == 'filter_tgl_bayar') {
			$this->db->where('b.tgl_bayar >=', $start_bayar);
			$this->db->where('b.tgl_bayar <=', $end_bayar);
		}elseif ($tipe_rekap == 'filter_tgl_setor') {
			$this->db->where('b.tgl_setor >=', $start_setor);
			$this->db->where('b.tgl_setor <=', $end_setor);
		}else{
			$this->db->where('r.id_rayon', 0);
		}

		$this->db->select('w.*, r.nama_rayon, rt.nama_rt');
		$this->db->from($this->table);
		$this->db->join('tb_rt rt', 'w.id_rt_ref=rt.id_rt');
		$this->db->join('tb_rayon r', 'r.id_rayon=rt.id_rayon_ref');
		$this->db->join('tb_bayar b', 'w.id_wp=b.id_wp_ref','left');

		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_rekap_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{	
		$tipe_rekap = $this->input->post('tipe_rekap');
		$id_rayon_ref = $this->input->post('id_rayon_ref');
		$id_rt_ref = $this->input->post('id_rt_ref');
		$bulan_bayar = $this->input->post('bulan_bayar') != '' ? $this->input->post('bulan_bayar') : NULL;
		$tgl_bayar = $this->input->post('tgl_bayar') != '' ? $this->input->post('tgl_bayar') : NULL;
		if ($tgl_bayar != NULL) {
			$tgl = explode(' - ' ,$this->input->post('tgl_bayar'));
			$start_bayar = date('Y-m-d', strtotime($tgl[0]));
			$end_bayar = date('Y-m-d', strtotime($tgl[1]));
		}

		$tgl_setor = $this->input->post('tgl_setor') != '' ? $this->input->post('tgl_setor') : NULL;
		if ($tgl_setor != NULL) {
			$tgl = explode(' - ' ,$this->input->post('tgl_setor'));
			$start_setor = date('Y-m-d', strtotime($tgl[0]));
			$end_setor = date('Y-m-d', strtotime($tgl[1]));
		}

		if ($tipe_rekap == 'filter_penerimaan_rayon') {
			$this->db->where('r.id_rayon', $id_rayon_ref);
			$this->db->where('rt.id_rt', $id_rt_ref);
		}elseif ($tipe_rekap == 'filter_bulan_rayon') {
			$this->db->where('r.id_rayon', $id_rayon_ref);
			$this->db->where('MONTH(b.tgl_bayar)', $bulan_bayar);
			$this->db->where('YEAR(b.tgl_bayar)', date('Y'));
		}elseif ($tipe_rekap == 'filter_tgl_rayon') {
			$this->db->where('r.id_rayon', $id_rayon_ref);
			$this->db->where('b.tgl_bayar >=', $start_bayar);
			$this->db->where('b.tgl_bayar <=', $end_bayar);
		}elseif ($tipe_rekap == 'filter_tgl_bayar') {
			$this->db->where('b.tgl_bayar >=', $start_bayar);
			$this->db->where('b.tgl_bayar <=', $end_bayar);
		}elseif ($tipe_rekap == 'filter_tgl_setor') {
			$this->db->where('b.tgl_setor >=', $start_setor);
			$this->db->where('b.tgl_setor <=', $end_setor);
		}else{
			$this->db->where('r.id_rayon', 0);
		}
		
		$this->db->join('tb_rt rt', 'w.id_rt_ref=rt.id_rt');
		$this->db->join('tb_rayon r', 'r.id_rayon=rt.id_rayon_ref');
		$this->db->join('tb_bayar b', 'w.id_wp=b.id_wp_ref','left');

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_rayon_option()
	{
		return $this->db->get('tb_rayon')->result_array();
	}

	public function get_rekap_download($tipe_rekap, $post){
		$id_rayon_ref = $post['id_rayon_ref'];
		$id_rt_ref = $post['id_rt_ref'];
		$bulan_bayar = $post['bulan_bayar'];
		$start_bayar = $post['start_bayar'];
		$end_bayar = $post['end_bayar'];
		$start_setor = $post['start_setor'];
		$end_setor = $post['end_setor'];

		$query = "SELECT wp.status, wp.id_wp, wp.nomor_wp, wp.pagu_wp, r.nama_rayon, b.tgl_bayar, b.tgl_setor FROM tb_wp wp 
				JOIN tb_rt rt ON wp.id_rt_ref=rt.id_rt
				JOIN tb_rayon r ON rt.id_rayon_ref=r.id_rayon
				LEFT JOIN tb_bayar b ON wp.id_wp=b.id_wp_ref
				WHERE wp.nomor_wp IS NOT NULL ";

		if ($tipe_rekap == 'filter_penerimaan_rayon') {
			$query .= "AND rt.id_rayon_ref = $id_rayon_ref AND rt.id_rt = $id_rt_ref";
			
		}elseif ($tipe_rekap == 'filter_bulan_rayon') {
			$tahun = date('Y');
			$query .= "AND rt.id_rayon_ref = $id_rayon_ref AND MONTH(b.tgl_bayar) = '$bulan_bayar' AND YEAR(b.tgl_bayar) = '$tahun'";
		}elseif ($tipe_rekap == 'filter_tgl_rayon') {
			$query .= "AND rt.id_rayon_ref = $id_rayon_ref AND b.tgl_bayar >= '$start_bayar' AND b.tgl_bayar <= '$end_bayar'";
		}elseif ($tipe_rekap == 'filter_tgl_bayar') {
			$query .= "AND b.tgl_bayar >= '$start_bayar' AND b.tgl_bayar <= '$end_bayar'";
		}else{
			$query .= "AND b.tgl_setor >= '$start_setor' AND b.tgl_setor <= '$end_setor'";
		}
		
		return $this->db->query($query)->result_array();
	}

	public function get_nama_rayon($id_rayon)
	{
		$query = "SELECT nama_rayon FROM tb_rayon WHERE id_rayon = ?";
		return $this->db->query($query, array($id_rayon))->row_array()['nama_rayon'];
	}

	public function get_setting()
	{
		$this->db->where('id_setting', 1);
		return $this->db->get('tb_setting')->row_array();
	}

	public function get_distinct_tahun_bayar()
	{
		$query = "SELECT DISTINCT YEAR(tgl_bayar) as tahun FROM tb_bayar";
		return $this->db->query($query)->result_array();
	}

	public function get_option_wilayah($id_rayon_ref)
	{
		$query = "SELECT id_rt, nama_rt FROM tb_rt WHERE id_rayon_ref = ?";
		return $this->db->query($query, array($id_rayon_ref))->result_array();
	}
}
 ?>