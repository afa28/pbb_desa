<?php 
/**
 * 
 */
class Setting_model extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	public function get_setting()
	{
		$this->db->where('id_setting', 1);
		return $this->db->get('tb_setting')->row_array();
	}

	public function add_setting($data)
	{
		$this->db->insert('tb_setting', $data);
	}

	public function update_setting($data)
	{
		$this->db->where('id_setting', 1);
		$this->db->update('tb_setting', $data);
	}
}
 ?>