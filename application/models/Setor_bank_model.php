<?php 
/**
 * 
 */
class Setor_bank_model extends CI_Model
{
	var $table = 'tb_bayar';
	var $column_order = array(null, 'tgl_bayar',null,'tgl_setor','tgl_setor');
	var $column_search = array('tgl_bayar','tgl_setor');
	var $order = array('id_bayar' => 'DESC');

	function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	private function _get_datatables_query()
	{	
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->group_by('tgl_bayar');

		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_setor_bank_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{	
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function update_bayar($data, $tgl_bayar)
	{
		$this->db->where('tgl_bayar', $tgl_bayar);
		$this->db->update('tb_bayar', $data);
	}
}
 ?>