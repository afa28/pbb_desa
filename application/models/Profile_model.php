<?php 
/**
 * 
 */
class Profile_model extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	public function get_login_username($id_login)
	{
		$query = "SELECT login_username FROM tb_login WHERE id_login = ?";
		return $this->db->query($query, array($id_login))->row_array()['login_username'];
	}

	public function cek_password_lama($old_login_password, $id_login)
	{
		$query = "SELECT id_login FROM tb_login WHERE id_login = ? AND login_password = ?";
		return $this->db->query($query, array($id_login, $old_login_password))->row_array()['id_login'];
	}

	public function update_login($data, $id_login)
	{
		$this->db->where('id_login', $id_login);
		$this->db->update('tb_login', $data);
	}
}
 ?>