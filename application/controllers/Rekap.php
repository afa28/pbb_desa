<?php 
/**
 * 
 */
class Rekap extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
		$this->load->model('rekap_model');
		$this->load->library('pdf');
	}

	public function index()
	{
		$header = array(
			'title' => 'Cetak Rekap',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$data['rayon'] = $this->rekap_model->get_rayon_option();
		$data['tahun_bayar'] = $this->rekap_model->get_distinct_tahun_bayar();

		$footer['page'] = 'rekap';

		$this->load->view('template/header', $header);
		$this->load->view('page/rekap', $data);
		$this->load->view('template/footer', $footer);
	}

	// Get Wp Datatable
	public function get_rekap()
	{
		$list = $this->rekap_model->get_rekap_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			if ($field->status == 2) {
				$field->status = '<span class="badge badge-success">Lunas</span>';
			}else{
				$field->status = '<span class="badge badge-danger">Terhutang</span>';
			}

			$row[] = $no;
			$row[] = $field->nomor_wp;
			$row[] = $field->nama_wp;
			$row[] = $field->nama_rayon != NULL ? $field->nama_rayon : '-';
			$row[] = $field->nama_rt != NULL ? $field->nama_rt : '-';
			$row[] = 'Rp. '.rupiah($field->pagu_wp);
			$row[] = $field->status;
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->rekap_model->count_all(),
			'recordsFiltered' => $this->rekap_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	public function get_option_wilayah($id_rayon_ref)
	{
		$data = $this->rekap_model->get_option_wilayah($id_rayon_ref);

		$option = '';
		if ($data != NULL) {
			foreach ($data as $key => $value) {
				$option .= '<option value="'.$value['id_rt'].'">'.$value['nama_rt'].'</option>';
			}
		}

		echo $option;
	}

	// Download Rekap SPPT
	public function download()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if ($_POST['tipe_rekap'] == 'filter_penerimaan_rayon') {
				$this->form_validation->set_rules('id_rayon_ref', 'Rayon', 'required');
				$this->form_validation->set_rules('id_rt_ref', 'RT', 'required');
			}elseif ($_POST['tipe_rekap'] == 'filter_bulan_rayon') {
				$this->form_validation->set_rules('id_rayon_ref', 'Rayon', 'required');
				$this->form_validation->set_rules('bulan_bayar', 'Bulan', 'required');
			}elseif ($_POST['tipe_rekap'] == 'filter_tgl_rayon') {
				$this->form_validation->set_rules('id_rayon_ref', 'Rayon', 'required');
				$this->form_validation->set_rules('tgl_bayar', 'Tanggal', 'required');
			}elseif ($_POST['tipe_rekap'] == 'filter_tgl_bayar') {
				$this->form_validation->set_rules('tgl_bayar', 'Tanggal', 'required');
			}else{
				$this->form_validation->set_rules('tgl_setor', 'Tanggal', 'required');
			}

			if ($this->form_validation->run() == true) {
				$tipe_rekap = $this->input->post('tipe_rekap');
				$id_rayon_ref = $this->input->post('id_rayon_ref');
				$id_rt_ref = $this->input->post('id_rt_ref');
				$start_bayar = '';
				$end_bayar = '';
				$start_setor = '';
				$end_setor = '';
				$bulan_bayar = $this->input->post('bulan_bayar') != '' ? $this->input->post('bulan_bayar') : NULL;
				$tgl_bayar = $this->input->post('tgl_bayar') != '' ? $this->input->post('tgl_bayar') : NULL;
				if ($tgl_bayar != NULL) {
					$tgl = explode(' - ' ,$this->input->post('tgl_bayar'));
					$start_bayar = date('Y-m-d', strtotime($tgl[0]));
					$end_bayar = date('Y-m-d', strtotime($tgl[1]));
				}

				$tgl_setor = $this->input->post('tgl_setor') != '' ? $this->input->post('tgl_setor') : NULL;
				if ($tgl_setor != NULL) {
					$tgl = explode(' - ' ,$this->input->post('tgl_setor'));
					$start_setor = date('Y-m-d', strtotime($tgl[0]));
					$end_setor = date('Y-m-d', strtotime($tgl[1]));
				}

				$post = array(
					'tipe_rekap' => $tipe_rekap,
					'id_rayon_ref' => $id_rayon_ref,
					'id_rt_ref' => $id_rt_ref,
					'bulan_bayar' => $bulan_bayar,
					'start_bayar' => $start_bayar,
					'end_bayar' => $end_bayar,
					'start_setor' => $start_setor,
					'end_setor' => $end_setor
				);
				
				$data['data'] = $this->rekap_model->get_rekap_download($tipe_rekap, $post);

				if ($tipe_rekap != 'filter_tgl_bayar' || $tipe_rekap != 'filter_tgl_setor') {
					$data['nama_rayon'] = $this->rekap_model->get_nama_rayon($id_rayon_ref);
				}

				$data['setting'] = $this->rekap_model->get_setting();
				
				$this->pdf->setPaper('A4', 'landscape');
			    $this->pdf->filename = 'Rekap SPPT.pdf';
				$this->pdf->load_view('print/rekap_sppt', $data);
			}else{
				$this->session->set_flashdata('input_error', 'Input Tidak Lengkap');
				redirect('rekap');
			}
		}
	}
}
 ?>