<?php 
/**
 * 
 */
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index()
	{
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (isset(cek_login()['status']) && cek_login()['status'] == true) {
        	redirect('/');
        }

		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('login_username', 'Username', 'required');
			$this->form_validation->set_rules('login_password', 'Password', 'required');

			if ($this->form_validation->run() == true) {
				$login_username = $this->input->post('login_username');
				$login_password = $this->input->post('login_password');

				$data_login = $this->login_model->get_data_login($login_username, sha1($login_password));
				if ($data_login == null) {
					$res = array(
						'status' => 'error',
						'message' => 'Username atau Password salah'
					);
					echo json_encode($res);
				}elseif ($data_login['status'] == 2) {
					$res = array(
						'status' => 'error',
						'message' => 'Akun Nonaktif'
					);
					echo json_encode($res);
				}else{
					$data_login['timestamp'] = now();
					$login_data = AUTHORIZATION::generateToken($data_login);
					
					setcookie('user_logged_in', $login_data, strtotime('+1 days'), '/');

					$res = array(
						'status' => 'success',
						'message' => 'Login Berhasil'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'login_username' => form_error('login_username'),
						'login_password' => form_error('login_password')
					)
				);
				echo json_encode($res);
			}
		}else{
			$this->load->view('login');
		}
	}

	public function logout()
	{
		// fungsi Logout
		unset($_COOKIE['user_logged_in']);
		setcookie("user_logged_in", null, -1, '/');
		redirect('login');
	}
}
 ?>