<?php 
/**
 * 
 */
class Dashboard extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login');
	    }
	    $this->load->model('dashboard_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Dashboard',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$data = array(
			'total_sppt' => $this->dashboard_model->get_total_sppt(),
			'total_rayon' => $this->dashboard_model->get_total_rayon(),
			'total_rt' => $this->dashboard_model->get_total_rt(),
			'total_pagu' => $this->dashboard_model->get_total_pagu(),
			'pagu_terhutang' => $this->dashboard_model->get_pagu_terhutang(),
			'pagu_terbayar' => $this->dashboard_model->get_pagu_terbayar(),
			'total_sppt_terhutang' => $this->dashboard_model->get_total_sppt_terhutang(),
			'total_sppt_terbayar' => $this->dashboard_model->get_total_sppt_terbayar(),
			'riwayat_pembayaran' => $this->dashboard_model->get_riwayat_pembayaran(),
			'rayon' => $this->dashboard_model->get_rayon()
		);

		$this->load->view('template/header', $header);
		$this->load->view('page/dashboard', $data);
		$this->load->view('template/footer');
	}
}
 ?>