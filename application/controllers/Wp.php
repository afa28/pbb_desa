<?php 
/**
 * 
 */

require_once FCPATH.'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
class Wp extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
	    $this->load->model('wp_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'SPPT',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$data['rayon'] = $this->wp_model->get_rayon_option();

		$footer['page'] = 'wp';

		$this->load->view('template/header', $header);
		$this->load->view('page/wp', $data);
		$this->load->view('template/footer', $footer);
	}

	// Get Wp Datatable
	public function get_wp()
	{
		$list = $this->wp_model->get_wp_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			if ($field->status == 1) {
				$field->status = '<span class="badge badge-danger">Terhutang</span>';
				$btn = '<button title="Edit" class="btn btn-sm btn-info before_edit_wp" id="beforeedit_'.$field->id_wp.'"><i class="fa fa-edit"></i></button> <button title="bayar" class="btn btn-sm btn-primary before_bayar_wp" id="bayar_'.$field->id_wp.'"><i class="fa fa-money"></i></button>';
			}else{
				$field->status = '<span class="badge badge-success">Lunas</span>';
				$btn = '<button class="btn btn-sm btn-danger before_batal_bayar_wp" id="batal_'.$field->id_wp.'" title="batal bayar"><i class="fa fa-money"></button>';
			}

			$row[] = $no;
			$row[] = $field->nomor_wp;
			$row[] = $field->nama_wp;
			$row[] = $field->nama_rayon != NULL ? $field->nama_rayon : '-';
			$row[] = $field->nama_rt != NULL ? $field->nama_rt : '-';
			$row[] = 'Rp. '.rupiah($field->pagu_wp);
			$row[] = $field->status;
			$row[] = $btn;
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->wp_model->count_all(),
			'recordsFiltered' => $this->wp_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('nomor_wp', 'Nomor Wajib Pajak', 'required|exact_length[18]');
			$this->form_validation->set_rules('nama_wp', 'Nama', 'required');
			$this->form_validation->set_rules('id_rt_ref', 'RT', 'required|numeric');
			$this->form_validation->set_rules('id_rayon', 'Rayon', 'required|numeric');
			$this->form_validation->set_rules('pagu_wp', 'Total Pajak', 'required|numeric');

			if ($this->form_validation->run() == true) {
				$nomor_wp = $this->input->post('nomor_wp');
				$nama_wp = $this->input->post('nama_wp');
				$id_rt_ref = $this->input->post('id_rt_ref');
				$pagu_wp = $this->input->post('pagu_wp');

				$this->db->trans_start();
				
				$data_wp = array(
					'nomor_wp' => $nomor_wp,
					'nama_wp' => $nama_wp,
					'id_rt_ref' => $id_rt_ref,
					'pagu_wp' => $pagu_wp,
					'status' => 1,
				);
				$id_wp = $this->wp_model->add_wp($data_wp);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Menambah Wp',
						'id_wp' => $id_wp
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Menambah Wp'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'nomor_wp' => form_error('nomor_wp'),
						'nama_wp' => form_error('nama_wp'),
						'id_rt_ref' => form_error('id_rt_ref'),
						'id_rayon' => form_error('id_rayon'),
						'pagu_wp' => form_error('pagu_wp')
					)
				);
				echo json_encode($res);
			}
		}
	}

	public function get_rt_option($id_rayon)
	{
		if ($id_rayon == NULL) {
			show_404();
			return false;
		}

		$res = '<option value="">Pilih RT</option>';
		
		$data_rt = $this->wp_model->get_rt_option($id_rayon);

		if ($data_rt != NULL) {
			foreach ($data_rt as $key => $value) {
				$res .= '<option value="'.$value['id_rt'].'">'.$value['nama_rt'].'</option>';
			}
		}

		echo $res;
	}

	public function get_detail_edit()
	{
		if ($_SERVER["REQUEST_METHOD"] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_wp', 'ID', 'required');

			if ($this->form_validation->run() == true) {
				$id_wp = $this->input->post('id_wp');

				$data_wp = $this->wp_model->get_detail_wp($id_wp);
				$rt = $this->wp_model->get_rt_option($data_wp['id_rayon']);

				$option_rt = '<option value="">Pilih RT</option>';
				if ($rt != NULL) {
					foreach ($rt as $key => $value) {
						$option_rt .= '<option value="'.$value['id_rt'].'">'.$value['nama_rt'].'</option>';
					}
				}

				if ($data_wp != NULL) {
					$res = array(
						'status' => 'success',
						'data' => $data_wp,
						'rt' => $option_rt
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Data Not Found'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Data Not Found'
				);
				echo json_encode($res);
			}
		}
	}

	public function update()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('nomor_wp', 'Nomor Wajib Pajak', 'required|exact_length[18]');
			$this->form_validation->set_rules('nama_wp', 'Nama', 'required');
			$this->form_validation->set_rules('id_rt_ref', 'RT', 'required|numeric');
			$this->form_validation->set_rules('id_rayon', 'Rayon', 'required|numeric');
			$this->form_validation->set_rules('pagu_wp', 'Total Pajak', 'required|numeric');
			$this->form_validation->set_rules('id_wp', 'ID Wp', 'required');

			if ($this->form_validation->run() == true) {
				$nomor_wp = $this->input->post('nomor_wp');
				$nama_wp = $this->input->post('nama_wp');
				$id_rt_ref = $this->input->post('id_rt_ref');
				$pagu_wp = $this->input->post('pagu_wp');
				$id_wp = $this->input->post('id_wp');

				$this->db->trans_start();
				
				$data_wp = array(
					'nomor_wp' => $nomor_wp,
					'nama_wp' => $nama_wp,
					'id_rt_ref' => $id_rt_ref,
					'pagu_wp' => $pagu_wp,
				);
				$this->wp_model->update_wp($data_wp, $id_wp);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Mengupdate SPPT'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Mengupdate SPPT'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'nomor_wp' => form_error('nomor_wp'),
						'nama_wp' => form_error('nama_wp'),
						'id_rt_ref' => form_error('id_rt_ref'),
						'id_rayon' => form_error('id_rayon'),
						'pagu_wp' => form_error('pagu_wp'),
						'id_wp' => form_error('id_wp')
					)
				);
				echo json_encode($res);
			}
		}
	}

	public function delete()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('id_wp', 'ID Wp', 'required');

			if ($this->form_validation->run() == true) {
				$id_wp = $this->input->post('id_wp');

				$this->db->trans_start();

				$this->wp_model->delete_wp($id_wp);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Menghapus SPPT'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Menghapus SPPT'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Gagal Menghapus SPPT'
				);
				echo json_encode($res);
			}
		}
	}

	public function bayar()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('id_wp', 'ID Wp', 'required');
			$this->form_validation->set_rules('tgl_bayar', 'Tanggal Bayar', 'required');

			if ($this->form_validation->run() == true) {
				$id_wp = $this->input->post('id_wp');
				$tgl_bayar = $this->input->post('tgl_bayar');

				$tgl_bayar = explode('/', $tgl_bayar);
				$tgl_bayar = date('Y-m-d', strtotime($tgl_bayar[2].'-'.$tgl_bayar[1].'-'.$tgl_bayar[0]));

				$this->db->trans_start();

				$data_bayar = array(
					'id_wp_ref' => $id_wp,
					'tgl_bayar' => $tgl_bayar,
					'status' => 2
				);
				$this->wp_model->add_bayar($data_bayar);

				$data_wp = array(
					'status' => 2
				);
				$this->wp_model->update_wp($data_wp, $id_wp);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Membayar SPPT'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Membayar SPPT'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Gagal Membayar SPPT'
				);
				echo json_encode($res);
			}
		}
	}

	public function batal_bayar()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('id_wp', 'ID Wp', 'required');

			if ($this->form_validation->run() == true) {
				$id_wp = $this->input->post('id_wp');

				$this->db->trans_start();

				$this->wp_model->delete_bayar($id_wp);

				$data_wp = array(
					'status' => 1
				);
				$this->wp_model->update_wp($data_wp, $id_wp);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Membatalkan pembayaran SPPT'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Membatalkan pembayaran SPPT'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Gagal Membatalkan pembayaran SPPT'
				);
				echo json_encode($res);
			}
		}
	}

	// Export Template SPPT
	public function export()
	{
		$spreadsheet = new Spreadsheet();
		
		//Sheet 1 Data SPPT-------------------------------------------------------------------------------------------
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('sppt');
		
		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(40);
		$sheet->getColumnDimension('C')->setWidth(20);
		$sheet->getColumnDimension('D')->setWidth(15);
		$sheet->getColumnDimension('E')->setWidth(20);
		
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Nomor SPPT');
		$sheet->setCellValue('C1', 'Nama Wajib Pajak');
		$sheet->setCellValue('D1', 'Total Pajak');
		$sheet->setCellValue('E1', 'RT');
		
		$count_rt = $this->wp_model->get_count_rt();

		$val_rt = $spreadsheet->getActiveSheet()
		->getCell('E2')
		->getDataValidation();
		$val_rt->setType(DataValidation::TYPE_LIST)
		->setErrorStyle(DataValidation::STYLE_INFORMATION)
		->setAllowBlank(false)
		->setShowInputMessage(false)
		->setShowErrorMessage(true)
		->setShowDropDown(true)
		->setFormula1('=rt!$B$2:$B$'.($count_rt+1));

		for ($i=1; $i <= 20; $i++) { 
			$sheet->setCellValue('A'.(1+$i), $i);
			$spreadsheet->getActiveSheet()->getCell('E'.(2+$i))->setDataValidation(clone $val_rt);

			$spreadsheet->getActiveSheet()
		    ->getStyle('B'.($i))
		    ->getNumberFormat()
		    ->setFormatCode(NumberFormat::FORMAT_TEXT);
		    $spreadsheet->getActiveSheet()
		    ->getStyle('D'.($i))
		    ->getNumberFormat()
		    ->setFormatCode(NumberFormat::FORMAT_TEXT);
		}

		//Sheet RT---------------------------------------------------------------------------------------
		$spreadsheet->createSheet();
		$spreadsheet->setActiveSheetIndex(1);
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('rt');

		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(20);

		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Nama RT');

		$rt = $this->wp_model->get_rt_option();

		if ($rt != null) {
			foreach ($rt as $key => $value) {
				$sheet->setCellValue('A'.(2+$key), (1+$key));
				$sheet->setCellValue('B'.(2+$key), $value['nama_rt'].'#'.$value['id_rt']);
			}
		}

		$spreadsheet->setActiveSheetIndex(0);

		//Writing Excel---------------------------------------------------------------------------------------------------
		$writer = new Xlsx($spreadsheet);

		ob_end_clean();
		header( "Content-type: application/vnd.ms-excel" );
		header('Content-Disposition: attachment; filename="Template_sppt_'.time().'.xlsx"');
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$writer->save('php://output');
		//End------------------------------------------------------------------------------------------------------------
	}

	// Import Data SPPT (function baru)
	public function import()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			if (isset($_FILES['userfile']['name'])) {
				//Upload FIle Excel-----------------------------------------------------------------------------------------------
	        	$name = 'Import_sppt_'.time();
	            $config['upload_path'] = './assets/import/';
	            $config['allowed_types'] = 'xls|xlsx';
	            $config['max_size'] = '2048';
	            $config['file_name'] = $name;

	            $this->load->library('upload', $config);

	            if (!$this->upload->do_upload('userfile')) {
	        		//Jika Upload Gagal-----------------------------------------------------------------------------------------
	                $res = array(
	                	'status' => 'error',
	                	'message' => strip_tags($this->upload->display_errors())
	                );
	                echo json_encode($res);
	                return false;
	            } else {
	        		//Jika Upload Berhasil---------------------------------------------------------------------------------------

	                $data = array('upload_data' => $this->upload->data());
	                $file = $this->upload->data('file_name');

		        	$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

		        	$spreadsheet = $reader->load(FCPATH.'/assets/import/'.$file);
		     		
		     		$spreadsheet->setActiveSheetIndex(0);
				    $sheetData = $spreadsheet->getActiveSheet()->toArray();

				    // Jika Ditemukan data untuk di masukan kedalam database
				    if ($sheetData != null && count($sheetData) > 2) {
				    	// Transaksi Database Dimulai
				    	$this->db->trans_start();
				    	foreach ($sheetData as $key => $value) {
				    		if ($key > 0) {
				    			if ($value[1] != NULL && $value[2] != NULL && $value[3] != NULL && $value[4] != NULL) {
				    				$nomor_wp = $value[1];
					    			$nama_wp = $value[2];
					    			$pagu_wp = $value[3];
					    			$id_rt_ref = explode('#', $value[4])[1];

					    			// Add WP
					    			$data_wp[$key] = array(
					    				'nomor_wp' => $nomor_wp,
					    				'nama_wp' => $nama_wp,
					    				'pagu_wp' => $pagu_wp,
					    				'id_rt_ref' => $id_rt_ref,
					    				'status' => 1
					    			);
					    			$this->wp_model->add_wp($data_wp[$key]);
				    			}
				    		}
				    	}
				    	// Transaksi Database Selesai
				    	$this->db->trans_complete();

	    				if ($this->db->trans_status() == true) {
	    					// Jika semua proses insert berhasil
	    					$res = array(
	    						'status' => 'success',
	    						'message' => 'Berhasil Mengimport Data SPPT'
	    					);
				    		echo json_encode($res);
	    				}else{
	    					// Jika Terdapat satu atau lebih error dalam proses insert data SPPT, akan dianggap error semua dan mencancel data yang sudah masuk sebelumnya
	    					$res = array(
	    						'status' => 'error',
	    						'message' => 'Gagal Mengimport Data SPPT, Terindikasi Data duplikat atau kesalahan entri data'
	    					);
				    		echo json_encode($res);
	    				}
				    }else{
				    	// Jika berhasil upload file excel namun tidak ditemukan data SPPT sama sekali
				    	$res = array(
				    		'status' => 'error',
				    		'message' => 'Data Not Found'
				    	);
				    	echo json_encode($res);
				    }
				}
			}else{
				// Jika Tidak ditemukan input file excel
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'userfile' => 'Excel file is required'
					)
				);
				echo json_encode($res);
			}
		}else{
			redirect('/');
		}
	}
}
 ?>