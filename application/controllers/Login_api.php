<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
/**
 * 
 */
class Login_api extends REST_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	// Check Validate Token
    public function index_get()
    {
        header('Content-Type: application/json');
        $auth = $this->get('jwt_key');
        if ($auth != null) {
            $decodedToken = AUTHORIZATION::validateTimestamp($auth);
            if ($decodedToken != false) {
                $res = array(
                    'status' => true,
                    'data' => $decodedToken
                );
                $this->set_response($res, REST_Controller::HTTP_OK);
                return;
            }else{
                $res = array(
                    'status' => false,
                    'message' => 'Unauthorized'
                );
                $this->set_response($res, REST_Controller::HTTP_UNAUTHORIZED);
            }
        }else{
            $res = array(
                'status' => false,
                'message' => 'Unauthorized'
            );
            $this->set_response($res, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
}
 ?>