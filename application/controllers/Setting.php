<?php 
/**
 * 
 */
class Setting extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login');
	    }
	    $this->load->model('setting_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Setting',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$data['setting'] = $this->setting_model->get_setting();

		$footer['page'] = 'setting';

		$this->load->view('template/header', $header);
		$this->load->view('page/setting', $data);
		$this->load->view('template/footer', $footer);
	}

	public function update()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('kode_provinsi', 'Kode Provinsi', 'required|numeric|max_length[5]');
			$this->form_validation->set_rules('nama_provinsi', 'Nama Provinsi', 'required');
			$this->form_validation->set_rules('kode_kabupaten', 'Kode Kabupaten', 'required|numeric|max_length[5]');
			$this->form_validation->set_rules('nama_kabupaten', 'Nama Kabupaten', 'required');
			$this->form_validation->set_rules('kode_kecamatan', 'Kode Kecamatan', 'required|numeric|max_length[5]');
			$this->form_validation->set_rules('nama_kecamatan', 'Nama Kecamatan', 'required');
			$this->form_validation->set_rules('kode_desa', 'Kode Desa', 'required|numeric|max_length[5]');
			$this->form_validation->set_rules('nama_desa', 'Nama Desa', 'required');

			if ($this->form_validation->run() == true) {
				$kode_provinsi = $this->input->post('kode_provinsi');
				$nama_provinsi = $this->input->post('nama_provinsi');
				$kode_kabupaten = $this->input->post('kode_kabupaten');
				$nama_kabupaten = $this->input->post('nama_kabupaten');
				$kode_kecamatan = $this->input->post('kode_kecamatan');
				$nama_kecamatan = $this->input->post('nama_kecamatan');
				$kode_desa = $this->input->post('kode_desa');
				$nama_desa = $this->input->post('nama_desa');
				$logo_surat = '';

	            $this->db->trans_start();

	            $data = array(
	            	'kode_provinsi' => $kode_provinsi,
	            	'nama_provinsi' => $nama_provinsi,
	            	'kode_kabupaten' => $kode_kabupaten,
	            	'nama_kabupaten' => $nama_kabupaten,
	            	'kode_kecamatan' => $kode_kecamatan,
	            	'nama_kecamatan' => $nama_kecamatan,
	            	'kode_desa' => $kode_desa,
	            	'nama_desa' => $nama_desa,
	            );

	            // cek setting
	            $setting = $this->setting_model->get_setting();
	            
	            // Upload Logo Surat
				if ($_FILES['logo_surat']['size'] > 0) {
					$name = 'logosurat-'.time();
		            $config['upload_path'] = './assets/logo/';
		            $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG|PNG';
		            $config['max_size'] = '2048';
		            $config['file_name'] = $name;

		            $this->load->library('upload', $config);

		            if (!$this->upload->do_upload('logo_surat')) {
		                $errors = array(
		                	'status' => 'error',
		                	'message' => strip_tags($this->upload->display_errors())
		                );
		                echo json_encode($errors);
		            } else {
		            	// Delete Logo Lama
		            	if ($setting['logo_surat'] != NULL && file_exists(FCPATH.'assets/logo/'.$setting['logo_surat'])) {
		            		unlink(FCPATH.'assets/logo/'.$setting['logo_surat']);
		            	}
		            	$logo_surat = $this->upload->data('file_name');
		                $data['logo_surat'] = $logo_surat;
		            }
				}

	            if ($setting != NULL) {
	            	// Update Setting
	            	$this->setting_model->update_setting($data);
	            }else{
	            	// Add Setting
	            	$this->setting_model->add_setting($data);
	            }

	            $this->db->trans_complete();

	            if ($this->db->trans_status() == true) {
	            	$res = array(
	            		'status' => 'success',
	            		'message' => 'Setting berhasil diupdate',
	            		'logo_surat' => $logo_surat
	            	);
	            	echo json_encode($res);
	            }else{
	            	$res = array(
	            		'status' => 'error',
	            		'message' => 'Setting gagal diupdate, kesalahan server'
	            	);
	            	echo json_encode($res);
	            }
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'kode_provinsi' => form_error('kode_provinsi'),
						'nama_provinsi' => form_error('nama_provinsi'),
						'kode_kabupaten' => form_error('kode_kabupaten'),
						'nama_kabupaten' => form_error('nama_kabupaten'),
						'kode_kecamatan' => form_error('kode_kecamatan'),
						'nama_kecamatan' => form_error('nama_kecamatan'),
						'kode_desa' => form_error('kode_desa'),
						'nama_desa' => form_error('nama_desa'),
						'logo_surat' => $_FILES['logo_surat']['size'] > 0 ? '' : 'The logo surat is required'
					)
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>