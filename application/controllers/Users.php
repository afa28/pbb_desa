<?php 
/**
 * 
 */
class Users extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
	    $this->load->model('users_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Users',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$footer['page'] = 'users';
		$this->load->view('template/header', $header);
		$this->load->view('page/users'); 
		$this->load->view('template/footer', $footer);
	}

	// Get Users Datatable
	public function get_users()
	{
		$list = $this->users_model->get_users_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $field->login_username;
			$row[] = 'Admin';
			$row[] = '<button title="Reset Password" class="btn btn-sm btn-info btn_reset_password" id="btnreset_'.$field->id_login.'"><i class="fa fa-lock"></i></button>';
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->users_model->count_all(),
			'recordsFiltered' => $this->users_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('login_username', 'Username', 'required|trim|max_length[20]');

			if ($this->form_validation->run() == true) {
				$login_username = $this->input->post('login_username');

				$this->db->trans_start();
				
				$data_login = array(
					'login_username' => $login_username,
					'login_password' => sha1('12345678'),
					'status' => 1,
				);
				$this->users_model->add_login($data_login);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Menambah Users'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Menambah Users'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'login_username' => form_error('login_username')
					)
				);
				echo json_encode($res);
			}
		}
	}

	public function reset_password()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_login', 'ID', 'required');

			if ($this->form_validation->run() == true) {
				$id_login = $this->input->post('id_login');

				$this->db->trans_start();

				$data_login = array(
					'login_password' => sha1('12345678')
				);
				$this->profile_model->update_login($data_login, $id_login);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Password Berhasil Direset'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Password Gagal Direset'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'ID Login Tidak Ditemukan'
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>