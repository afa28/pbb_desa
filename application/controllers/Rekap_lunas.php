<?php 
/**
 * 
 */
class Rekap_lunas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login');
	    }
	    $this->load->model('rekap_lunas_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Rekap Lunas',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$footer['page'] = 'rekap_lunas';

		$this->load->view('template/header', $header);
		$this->load->view('page/rekap_lunas');
		$this->load->view('template/footer', $footer);
	}

	// Get Wp Datatable
	public function get_rekap()
	{
		$list = $this->rekap_lunas_model->get_rekap_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			if ($field->status == 2) {
				$field->status = '<span class="badge badge-success">Lunas</span>';
			}else{
				$field->status = '<span class="badge badge-danger">Terhutang</span>';
			}

			$row[] = $no;
			$row[] = $field->nomor_wp;
			$row[] = $field->nama_wp;
			$row[] = $field->nama_rayon != NULL ? $field->nama_rayon : '-';
			$row[] = $field->nama_rt != NULL ? $field->nama_rt : '-';
			$row[] = 'Rp. '.rupiah($field->pagu_wp);
			$row[] = $field->status;
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->rekap_lunas_model->count_all(),
			'recordsFiltered' => $this->rekap_lunas_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}
}
 ?>