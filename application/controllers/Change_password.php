<?php 
/**
 * 
 */
class Change_password extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
	    $this->load->model('profile_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Change Password',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$footer['page'] = 'change_password';
		$this->load->view('template/header', $header);
		$this->load->view('page/change_password'); 
		$this->load->view('template/footer', $footer);
	}

	public function update()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('old_login_password', 'Password Lama', 'required');
			$this->form_validation->set_rules('new_login_password', 'Password Baru', 'required|min_length[8]');
			$this->form_validation->set_rules('renew_login_password', 'Password Baru Ulang', 'required|matches[new_login_password]');

			if ($this->form_validation->run() == true) {
				$old_login_password = $this->input->post('old_login_password');
				$new_login_password = $this->input->post('new_login_password');
				$id_login = data_login('id_login');

				$cek_password_lama = $this->profile_model->cek_password_lama(sha1($old_login_password), $id_login);

				if ($cek_password_lama == NULL) {
					$res = array(
						'status' => 'error',
						'message' => 'Password Lama Tidak Cocok'
					);
					echo json_encode($res);
					return false;
				}

				$this->db->trans_start();
				
				$data_login = array(
					'login_password' => sha1($new_login_password)
				);
				$this->profile_model->update_login($data_login, $id_login);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Mengubah Password'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Mengubah Password'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'old_login_password' => form_error('old_login_password'),
						'new_login_password' => form_error('new_login_password'),
						'renew_login_password' => form_error('renew_login_password')
					)
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>