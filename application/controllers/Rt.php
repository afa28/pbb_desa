<?php 
/**
 * 
 */
class Rt extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
	    $this->load->model('rt_model');
	    $this->load->model('detail_rt_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'RT',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$data['rayon'] = $this->rt_model->get_rayon_option();

		$footer['page'] = 'rt';

		$this->load->view('template/header', $header);
		$this->load->view('page/rt', $data);
		$this->load->view('template/footer', $footer);
	}

	// Get RT Datatable
	public function get_rt()
	{
		$list = $this->rt_model->get_rt_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			// Total Pagu
			$query_pagu = "SELECT sum(w.pagu_wp) as total_pagu FROM tb_wp w
							JOIN tb_rt rt ON rt.id_rt=w.id_rt_ref
							WHERE rt.id_rt = ?";
			$total_pagu = $this->db->query($query_pagu, array($field->id_rt))->row_array()['total_pagu'];

			// Total Bayar
			$query_bayar = "SELECT sum(w.pagu_wp) as total_bayar FROM tb_wp w
							JOIN tb_rt rt ON rt.id_rt=w.id_rt_ref
							WHERE rt.id_rt = ? AND w.status = ?";
			$total_bayar = $this->db->query($query_bayar, array($field->id_rt, 2))->row_array()['total_bayar'];

			// Total Kekurangan
			$total_kekurangan = $total_pagu-$total_bayar;

			if ($total_bayar > 0 && $total_pagu > 0) {
				$persen_pagu_terpenuhi = round($total_bayar/$total_pagu*100,2);
			}else{
				$persen_pagu_terpenuhi = 0;
			}

			$pagu_terpenuhi = '<div class="progress progress-xs mt-1">
                        <div class="progress-bar bg-success progress-bar-striped" style="width: '.$persen_pagu_terpenuhi.'%"></div>
                        <span class="text-success">'.$persen_pagu_terpenuhi.'%</span>';

			$row[] = $no;
			$row[] = $field->nama_rt;
			$row[] = $field->nama_rayon != NULL ? $field->nama_rayon : '-';
			$row[] = 'Rp. '.rupiah($total_pagu);
			$row[] = '<span class="text-success">Rp. '.rupiah($total_bayar).'</span>';
			$row[] = '<span class="text-danger">Rp. '.rupiah($total_kekurangan).'</span>';
			$row[] = $pagu_terpenuhi;
			$row[] = '<button title="Detail" class="btn btn-info btn-sm btn_detail_rt" id="detailrt_'.$field->id_rt.'"><i class="fa fa-info"></i></button> <button title="Edit" class="btn btn-sm btn-info before_edit_rt" id="beforeedit_'.$field->id_rt.'"><i class="fa fa-edit"></i></button> <button title="Delete" class="btn btn-sm btn-danger before_delete_rt" id="beforedelete_'.$field->id_rt.'"><i class="fa fa-trash"></i></button>';
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->rt_model->count_all(),
			'recordsFiltered' => $this->rt_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	// Get Detail Rayon Datatable
	public function get_detail_rt()
	{
		$list = $this->detail_rt_model->get_detail_rt_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			if ($field->status == 1) {
				$field->status = '<span class="badge badge-danger">Terhutang</span>';
			}else{
				$field->status = '<span class="badge badge-success">Lunas</span>';
			}

			$row[] = $no;
			$row[] = $field->nama_wp;
			$row[] = $field->nama_rt;
			$row[] = 'Rp. '.rupiah($field->pagu_wp);
			$row[] = $field->status;

			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->detail_rt_model->count_all(),
			'recordsFiltered' => $this->detail_rt_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('nama_rt', 'Nama', 'required');
			$this->form_validation->set_rules('id_rayon_ref', 'Rayon', 'required');

			if ($this->form_validation->run() == true) {
				$nama_rt = $this->input->post('nama_rt');
				$id_rayon_ref = $this->input->post('id_rayon_ref');

				$this->db->trans_start();
				
				$data_rt = array(
					'nama_rt' => $nama_rt,
					'id_rayon_ref' => $id_rayon_ref
				);
				$id_rt = $this->rt_model->add_rt($data_rt);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Menambah RT',
						'id_rt' => $id_rt
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Menambah RT'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'nama_rt' => form_error('nama_rt'),
						'id_rayon_ref' => form_error('id_rayon_ref')
					)
				);
				echo json_encode($res);
			}
		}
	}

	
	public function get_detail_edit()
	{
		if ($_SERVER["REQUEST_METHOD"] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_rt', 'ID', 'required');

			if ($this->form_validation->run() == true) {
				$id_rt = $this->input->post('id_rt');

				$data_rt = $this->rt_model->get_detail_rt($id_rt);

				if ($data_rt != NULL) {
					$res = array(
						'status' => 'success',
						'data' => $data_rt
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Data Not Found'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Data Not Found'
				);
				echo json_encode($res);
			}
		}
	}

	public function update()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('id_rayon_ref', 'Rayon', 'required');
			$this->form_validation->set_rules('nama_rt', 'Nama', 'required');
			$this->form_validation->set_rules('id_rt', 'ID RT', 'required');

			if ($this->form_validation->run() == true) {
				$id_rayon_ref = $this->input->post('id_rayon_ref');
				$nama_rt = $this->input->post('nama_rt');
				$id_rt = $this->input->post('id_rt');

				$this->db->trans_start();
				
				$data_rt = array(
					'nama_rt' => $nama_rt,
					'id_rayon_ref' => $id_rayon_ref
				);
				$this->rt_model->update_rt($data_rt, $id_rt);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Mengupdate RT'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Mengupdate RT'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'nama_rt' => form_error('nama_rt'),
						'id_rayon_ref' => form_error('id_rayon_ref')
					)
				);
				echo json_encode($res);
			}
		}
	}

	public function delete()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_rt', 'ID', 'required|numeric');

			if ($this->form_validation->run() == true) {
				$id_rt = $this->input->post('id_rt');

				$this->db->trans_start();

				$this->rt_model->delete_rt($id_rt);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'RT berhasil dihapus'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'RT gagal dihapus'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'ID tidak ditemukan'
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>