<?php 
/**
 * 
 */
class Input_pembayaran extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login');
	    }
	    $this->load->model('wp_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Input Pembayaran',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$data['rayon'] = $this->wp_model->get_rayon_option();

		$footer['page'] = 'input_pembayaran';

		$this->load->view('template/header', $header);
		$this->load->view('page/input_pembayaran', $data);
		$this->load->view('template/footer', $footer);
	}

	public function get_wp_option()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('id_rayon_ref', 'Rayon', 'required');

			if ($this->form_validation->run() == true) {
				$id_rayon_ref = $this->input->post('id_rayon_ref');

				$data = $this->wp_model->get_wp_option($id_rayon_ref);

				if ($data != NULL) {
					$option = '<option value="">Pilih Nama Wajib Pajak</option>';
					foreach ($data as $key => $value) {
						$option .= '<option value="'.$value['id_wp'].'">'.$value['nomor_wp'].' - '.$value['nama_wp'].'</option>';
					}

					$res = array(
						'status' => 'success',
						'option' => $option
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'No Data'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Rayon Tidak Ditemukan'
				);
				echo json_encode($res);
			}
		}
	}

	public function get_wp_detail()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_wp', 'Nama Wajib Pajak', 'required');

			if ($this->form_validation->run() == true) {
				$id_wp = $this->input->post('id_wp');

				$data = $this->wp_model->get_detail_wp($id_wp);

				if ($data != NULL) {
					$data['pagu_wp'] = 'Rp. '.rupiah($data['pagu_wp']);
					$res = array(
						'status' => 'success',
						'data' => $data
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'No Data'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Nama Wajib Pajak Tidak Ditemukan'
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>