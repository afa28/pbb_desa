<?php 
/**
 * 
 */
class Rekap_waktu extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
	    $this->load->model('setor_bank_model');
	    $this->load->model('detail_setor_bank_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Rekap Waktu',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$footer['page'] = 'setor_bank';

		$this->load->view('template/header', $header);
		$this->load->view('page/rekap_waktu');
		$this->load->view('template/footer', $footer);
	}

	// Get Rekap Waktu Datatable
	public function get_setor_bank()
	{
		$list = $this->setor_bank_model->get_setor_bank_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			$query = "SELECT SUM(pagu_wp) as jumlah_penerimaan from tb_wp w join tb_bayar b on w.id_wp=b.id_wp_ref where b.tgl_bayar= ?";
			$jumlah_penerimaan = $this->db->query($query, array($field->tgl_bayar))->row_array()['jumlah_penerimaan'];

			if ($field->tgl_setor == NULL) {
				$status = '<span class="badge badge-danger">Terhutang</span>';
					      
				$tgl_setor = '<input type="hidden" id="tgl_bayar_'.$field->id_bayar.'" name="tgl_bayar" value="'.$field->tgl_bayar.'"><button title="Setor" class="btn btn-sm btn-success before_setor_bank" id="setorbank_'.$field->tgl_bayar.'"><i class="fa fa-money"></i> Setor</button>';
			}else{
				$status = '<span class="badge badge-success">Lunas</span>';
				$tgl_setor = '<input type="hidden" id="tgl_bayar_'.$field->id_bayar.'" name="tgl_bayar" value="'.$field->tgl_bayar.'">'.tgl_indo($field->tgl_setor).' <button title="Batalkan" class="btn btn-sm btn-danger btn_batalkan_setor_bank" id="batalkan_'.$field->tgl_bayar.'"><i class="fa fa-times"></i></button>';
			}

			$row[] = $no;
			$row[] = '<button class="btn btn-sm btn-info btn_detail_setor_bank" id="bayar_'.$field->id_bayar.'">'.tgl_indo($field->tgl_bayar).'</button>';
			$row[] = '<span class="text-success">Rp. '.rupiah($jumlah_penerimaan).'</span>';
			$row[] = $status;
			$row[] = $tgl_setor;
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->setor_bank_model->count_all(),
			'recordsFiltered' => $this->setor_bank_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	// Get Detail Rekap Waktu Datatable
	public function get_detail_setor_bank()
	{
		$list = $this->detail_setor_bank_model->get_detail_setor_bank_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			if ($field->status == 1) {
				$field->status = '<span class="badge badge-danger">Terhutang</span>';
			}else{
				$field->status = '<span class="badge badge-success">Lunas</span>';
			}

			$row[] = $no;
			$row[] = $field->nama_wp;
			$row[] = $field->nama_rt;
			$row[] = 'Rp. '.rupiah($field->pagu_wp);
			$row[] = $field->status;

			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->detail_setor_bank_model->count_all(),
			'recordsFiltered' => $this->detail_setor_bank_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	public function setor()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('tgl_bayar', 'Tanggal Bayar', 'required');
			$this->form_validation->set_rules('tgl_setor', 'Tanggal Setor', 'required');

			if ($this->form_validation->run() == true) {
				$tgl_bayar = $this->input->post('tgl_bayar');
				$tgl_setor = $this->input->post('tgl_setor');

				$tgl_setor = explode('/', $tgl_setor);
				$tgl_setor = date('Y-m-d', strtotime($tgl_setor[2].'-'.$tgl_setor[1].'-'.$tgl_setor[0]));

				$this->db->trans_start();
				
				$data_bayar = array(
					'tgl_setor' => $tgl_setor
				);
				$id_rayon = $this->setor_bank_model->update_bayar($data_bayar, $tgl_bayar);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Melakukan Rekap Waktu'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Melakukan Rekap Waktu'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'tgl_bayar' => form_error('tgl_bayar'),
						'tgl_setor' => form_error('tgl_setor')
					)
				);
				echo json_encode($res);
			}
		}
	}

	public function batalkan()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('tgl_bayar', 'Tanggal Bayar', 'required');

			if ($this->form_validation->run() == true) {
				$tgl_bayar = $this->input->post('tgl_bayar');

				$this->db->trans_start();
				
				$data_bayar = array(
					'tgl_setor' => NULL
				);
				$id_rayon = $this->setor_bank_model->update_bayar($data_bayar, $tgl_bayar);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Membatalkan Rekap Waktu'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Membatalkan Rekap Waktu'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Gagal Membatalkan Rekap Waktu'
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>