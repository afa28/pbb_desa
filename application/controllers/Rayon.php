<?php 
/**
 * 
 */
class Rayon extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// Cek Apakah cookie login sudah ada dan terverifikasi atau belum
		if (!isset(cek_login()['status']) || cek_login()['status'] == false || cek_login() == "NoData") {
	    	redirect('login/logout');
	    }
	    $this->load->model('rayon_model');
	    $this->load->model('detail_rayon_model');
	}

	public function index()
	{
		$header = array(
			'title' => 'Rayon',
			'login_username' => $this->profile_model->get_login_username(data_login('id_login'))
		);

		$footer['page'] = 'rayon';

		$this->load->view('template/header', $header);
		$this->load->view('page/rayon');
		$this->load->view('template/footer', $footer);
	}

	// Get Rayon Datatable
	public function get_rayon()
	{
		$list = $this->rayon_model->get_rayon_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			// Total Pagu
			$total_pagu = $this->rayon_model->get_total_pagu_per_rayon($field->id_rayon);

			// Total Bayar
			$total_bayar = $this->rayon_model->get_total_bayar_per_rayon($field->id_rayon);

			// Total Kekurangan
			$total_kekurangan = $total_pagu-$total_bayar;

			if ($total_bayar > 0 && $total_pagu > 0) {
				$persen_pagu_terpenuhi = round($total_bayar/$total_pagu*100,2);
			}else{
				$persen_pagu_terpenuhi = 0;
			}

			$pagu_terpenuhi = '<div class="progress progress-xs mt-1">
                        <div class="progress-bar bg-success progress-bar-striped" style="width: '.$persen_pagu_terpenuhi.'%"></div>
                        <span class="text-success">'.$persen_pagu_terpenuhi.'%</span>';

			$row[] = $no;
			$row[] = $field->nama_rayon;
			$row[] = 'Rp. '.rupiah($total_pagu);
			$row[] = '<span class="text-success">Rp. '.rupiah($total_bayar).'</span>';
			$row[] = '<span class="text-danger">Rp. '.rupiah($total_kekurangan).'</span>';
			$row[] = $pagu_terpenuhi;
			$row[] = '<button title="Detail" class="btn btn-info btn-sm btn_detail_rayon" id="detailrayon_'.$field->id_rayon.'"><i class="fa fa-info"></i></button> <button title="Edit" class="btn btn-sm btn-info before_edit_rayon" id="beforeedit_'.$field->id_rayon.'"><i class="fa fa-edit"></i></button> <button title="Delete" class="btn btn-sm btn-danger before_delete_rayon" id="beforedelete_'.$field->id_rayon.'"><i class="fa fa-trash"></i></button>';
			
			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->rayon_model->count_all(),
			'recordsFiltered' => $this->rayon_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	// Get Detail Rayon Datatable
	public function get_detail_rayon()
	{
		$list = $this->detail_rayon_model->get_detail_rayon_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();

			if ($field->status == 1) {
				$field->status = '<span class="badge badge-danger">Terhutang</span>';
			}else{
				$field->status = '<span class="badge badge-success">Lunas</span>';
			}

			$row[] = $no;
			$row[] = $field->nama_wp;
			$row[] = $field->nama_rt;
			$row[] = 'Rp. '.rupiah($field->pagu_wp);
			$row[] = $field->status;

			$data[] = $row;
		}

		$output = array(
			'draw' => $_POST['draw'],
			'recordsTotal' => $this->detail_rayon_model->count_all(),
			'recordsFiltered' => $this->detail_rayon_model->count_filtered(),
			'data' => $data
		);
		echo json_encode($output);
	}

	public function get_timeline_rekap($id_rayon_ref)
	{
		$data = $this->rayon_model->get_riwayat_pembayaran($id_rayon_ref);
		$res = '<hr>
                <h5>Timeline Rekap Pembayaran</h5>
                <ul class="timeline">';

        if ($data != NULL) {
        	foreach ($data as $key => $value) {
        		$res .= '<li>
			        		<a href="#">'.$value['nama_wp'].' telah melunasi pajaknya.</a>
			                <a href="#" class="float-right">'.tgl_indo($value['tgl_bayar']).'</a>
        				</li>';
        	}
        }else{
        	$res .= '<li>
		                <a href="#">No Record</a>
		                <a href="#" class="float-right"></a>
		            </li>';
        }

        $res .=  '</ul>';

         echo $res;
	}

	public function add()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('nama_rayon', 'Nama', 'required');

			if ($this->form_validation->run() == true) {
				$nama_rayon = $this->input->post('nama_rayon');

				$this->db->trans_start();
				
				$data_rayon = array(
					'nama_rayon' => $nama_rayon
				);
				$id_rayon = $this->rayon_model->add_rayon($data_rayon);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Menambah Rayon',
						'id_rayon' => $id_rayon
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Menambah Rayon'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'nama_rayon' => form_error('nama_rayon')
					)
				);
				echo json_encode($res);
			}
		}
	}

	
	public function get_detail_edit()
	{
		if ($_SERVER["REQUEST_METHOD"] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_rayon', 'ID', 'required');

			if ($this->form_validation->run() == true) {
				$id_rayon = $this->input->post('id_rayon');

				$data_rayon = $this->rayon_model->get_detail_rayon($id_rayon);

				if ($data_rayon != NULL) {
					$res = array(
						'status' => 'success',
						'data' => $data_rayon
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Data Not Found'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'Data Not Found'
				);
				echo json_encode($res);
			}
		}
	}

	public function update()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_error_delimiters('','');
			$this->form_validation->set_rules('nama_rayon', 'Nama', 'required');
			$this->form_validation->set_rules('id_rayon', 'ID Rayon', 'required');

			if ($this->form_validation->run() == true) {
				$nama_rayon = $this->input->post('nama_rayon');
				$id_rayon = $this->input->post('id_rayon');

				$this->db->trans_start();
				
				$data_rayon = array(
					'nama_rayon' => $nama_rayon
				);
				$this->rayon_model->update_rayon($data_rayon, $id_rayon);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Berhasil Mengupdate Rayon'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Gagal Mengupdate Rayon'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'valid_error',
					'error' => array(
						'nama_rayon' => form_error('nama_rayon')
					)
				);
				echo json_encode($res);
			}
		}
	}

	public function delete()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->input->is_ajax_request()) {
			$this->form_validation->set_rules('id_rayon', 'ID', 'required|numeric');

			if ($this->form_validation->run() == true) {
				$id_rayon = $this->input->post('id_rayon');

				$this->db->trans_start();

				$this->rayon_model->delete_rayon($id_rayon);

				$this->db->trans_complete();

				if ($this->db->trans_status() == true) {
					$res = array(
						'status' => 'success',
						'message' => 'Rayon berhasil dihapus'
					);
					echo json_encode($res);
				}else{
					$res = array(
						'status' => 'error',
						'message' => 'Rayon gagal dihapus'
					);
					echo json_encode($res);
				}
			}else{
				$res = array(
					'status' => 'error',
					'message' => 'ID tidak ditemukan'
				);
				echo json_encode($res);
			}
		}
	}
}
 ?>